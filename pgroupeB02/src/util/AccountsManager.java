package util;

import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.reflect.TypeToken;

import modele.Account;

public abstract class AccountsManager 
{
	private static List<Account> accounts;
	/**
	 * Add an account
	 * @param p the account to add 
	 */
	public static void addAccount(Account p)
	{
		if(accounts == null)
		{
			accounts = new ArrayList<>();
		}
		
		if(!accounts.contains(p))
			accounts.add(p);
	}
	
	/**
	 * Set the list of accounts
	 * @param c the list of accounts
	 */
	public static void setAccounts(List<Account> c)
	{
		accounts = c;
		saveAccounts();
	}
	
	/**
	 * Return a player
	 * @param nickname the player nickname
	 * @return the player
	 */
	public static Account getPlayer(String nickname)
	{		
		for (Account player : accounts) 
		{
			if(player.getNickname().equalsIgnoreCase(nickname))
			{
				return player;
			}
		}
		
		return null;
	}
	
	/**
	 * Return the accounts list
	 * @return the list of accounts
	 */
	public static List<Account> getAccounts()
	{
		return accounts;
	}
	
	/**
	 * Save the accounts into a json file
	 */
	public static void saveAccounts()
	{
		//Write the accounts into the json file.
		
		//Make use of a custom type to ensure the good serialization of javafx properties
		Type type = new TypeToken<List<Account>>() {}.getType();    
		Serialization.writeToJson(accounts, "accounts",type);
	}
	
	/**
	 * Load the account list from a json file
	 * @return Return true if the file is correctly loaded
	 */
	@SuppressWarnings("unchecked")
	public static boolean loadAccounts()
	{		
		//Read the accounts from the json file.
		
		//Make use of a custom type to ensure the good serialization of javafx properties
		Type type = new TypeToken<List<Account>>() {}.getType();    
		try {
			accounts = ((List<Account>)Serialization.readFromJson("accounts", type));
			System.out.println(accounts);
			return true;
		} catch (FileNotFoundException e) {
			return false;
		}
	}
	public static boolean removeAccount(Account c) {
		return accounts.remove(c);
		
	}
}
