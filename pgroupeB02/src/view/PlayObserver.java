package view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.StageStyle;
import modele.GameObservable;
import modele.JokerCallFriend;
import modele.JokerCallPublic;
import modele.JokerFiftyFifty;
import modele.Observable;
import modele.Palier;
import modele.State;

public class PlayObserver extends BorderPane implements Observer{
	private Boolean useJK50,useJKPublic,useJKTimer,useJKAmis;
	private Button btnBack,btnJoker50,btnJokerPublic,btnJokerTimer,btnJokerAmis,btnLeave;
	private static final double jokerSizeWidth=150;
	private static final double jokerSizeHeight=30;
	private GameObservable game;
	private Group grpButton;
	private int compteurTime,decrease = 0;
	private Label lblStatement,lblState,timer,lblCorrectAnswer,lblHelpFriend;
	private List<Button> listChoix;
	private List<Label> listPalier,listAidePublic;
	private VBox vbPalier;
	private HBox jokerBox;
	private Dialog dialog;
	private DialogPane dialogPane;
	private ButtonType okButtonType;
	@Override
	public void actualise(Observable o) {
		
		switch(getGame().getEtat()) {
		case LOSE:
			getLblState().setText("YOU LOST :"+getGame().getMoneySave()+" �");
			getLblState().setStyle("-fx-text-fill: red;");
			disable(getGame().getCorrectAnswer());
			getLblStatement().setVisible(false);
			getTimer().setVisible(false);
			dialog = new Dialog();
			dialogPane = dialog.getDialogPane();
			dialogPane.getStylesheets().add(
			   getClass().getResource("Buttons.css").toExternalForm());
			dialogPane.getStyleClass().add("myDialog");
			dialog.setTitle("Game Finished");
			okButtonType = new ButtonType("Oh no !", ButtonData.CANCEL_CLOSE);
			dialog.getDialogPane().getButtonTypes().addAll(okButtonType);
			dialog.initStyle(StageStyle.UNDECORATED);
			dialog.getDialogPane().setContent(getLblState());
			this.setEffect(new GaussianBlur());
			dialog.showAndWait();
			this.setEffect(null);
			setDecrease(false);	
			break;
		case WIN:
			dialog = new Dialog();
			dialogPane = dialog.getDialogPane();
			dialogPane.getStylesheets().add(
			   getClass().getResource("Buttons.css").toExternalForm());
			dialogPane.getStyleClass().add("myDialog");
			getLblState().setText("YOU WON "+getGame().getMoneySave()+" �");
			dialog.setTitle("Game Finished");
			dialog.initStyle(StageStyle.UNDECORATED);
			okButtonType = new ButtonType("Great !", ButtonData.CANCEL_CLOSE);
			dialog.getDialogPane().getButtonTypes().addAll(okButtonType);
			dialog.getDialogPane().setContent(getLblState());
			disable();
			setDecrease(false);
			getLblStatement().setText("");
			getTimer().setVisible(false);
			getLblStatement().setVisible(false);
			this.setEffect(new GaussianBlur());
			dialog.showAndWait();
			this.setEffect(null);
			break;
		default:
			setGame((GameObservable)o);
			break;
		
		}
	}
	public void disable() {
		for(Button v:getListChoix()) {
			v.setVisible(false);
		}
		
		useJK50=true;
		useJKAmis=true;
		useJKTimer=true;
		useJKPublic=true;
		getJokers().setVisible(false);
	}
	public void disable(String str) {
		for(Button v:getListChoix()) {
			v.setVisible(false);
		}
		useJK50=true;
		useJKAmis=true;
		useJKTimer=true;
		useJKPublic=true;
		getJokers().setVisible(false);
		
		getLblCorrectAnswer().setText("the correct answer was :\""+str+"\"");
		if(!str.isEmpty())
			getLblCorrectAnswer().setFont(Font.font("Verdana",FontWeight.BOLD,13));
			getLblCorrectAnswer().getStyleClass().add("statement");
			getLblCorrectAnswer().setVisible(true);
			
	}
	public void enable() {
		getLblCorrectAnswer().setVisible(false);
		for(Button v:getListChoix()) {
			v=new Button();
			v.setVisible(true);
		}
		System.out.println("reload");
		useJK50=false;
		useJKAmis=false;
		useJKTimer=false;
		useJKPublic=false;
		getJokers().setVisible(true);
	}
	public Button getBtnBack() {
		if(btnBack==null) {
			btnBack=new Button("Back to menu");
			btnBack.setTextAlignment(TextAlignment.CENTER);
			btnBack.setWrapText(true);
			btnBack.setMaxWidth(150);
			btnBack.getStyleClass().clear();
			btnBack.getStyleClass().add("record-sales");
		}
		return btnBack;
	}
	

	
	
	public Button getBtnJokerAmis() {
		if(btnJokerAmis==null) {
			btnJokerAmis=new Button("Call a friend");
			btnJokerAmis.getStyleClass().add("shiny-orange");
			btnJokerAmis.setOnAction(e->{
					getGame().setJoker(new JokerCallFriend());
					btnJokerAmis.getStyleClass().add("used");
			});
			btnJokerAmis.setMinWidth(jokerSizeWidth);
			btnJokerAmis.setWrapText(true);
			btnJokerAmis.setMinHeight(jokerSizeHeight);
			btnJokerAmis.setTextAlignment(TextAlignment.CENTER);
		}
		return btnJokerAmis;
	}
	public Button getBtnJokerPublic() {
		if(btnJokerPublic==null) {
			btnJokerPublic=new Button("help from people");
			btnJokerPublic.getStyleClass().add("shiny-orange");
			btnJokerPublic.setOnAction(e->{
				if(!useJKPublic) {
					getGame().setJoker(new JokerCallPublic());
					btnJokerPublic.getStyleClass().add("used");
				}
			});
			btnJokerPublic.setWrapText(true);
			btnJokerPublic.setMinWidth(jokerSizeWidth);
			btnJokerPublic.setMinHeight(jokerSizeHeight);
			btnJokerPublic.setTextAlignment(TextAlignment.CENTER);
		}
		return btnJokerPublic;
	}
	public Button getBtnJokerTimer() {
		if(btnJokerTimer==null) {
			btnJokerTimer=new Button("Add aditionnal time");
			btnJokerTimer.getStyleClass().add("shiny-orange");
			btnJokerTimer.setOnAction(e->{
				if(!useJKTimer) {
					compteurTime+=45;
					useJKTimer=true;
					btnJokerTimer.getStyleClass().add("used");
				}
			});
			btnJokerTimer.setMinWidth(jokerSizeWidth);
			btnJokerTimer.setMinHeight(jokerSizeHeight);
			btnJokerTimer.setWrapText(true);
			btnJokerTimer.setTextAlignment(TextAlignment.CENTER);
		}
		return btnJokerTimer;
	}
	public Button getBtnJoker50() {
		if(btnJoker50==null) {
			btnJoker50=new Button("50-50");
			btnJoker50.getStyleClass().add("shiny-orange");
			btnJoker50.setOnAction(e->{
				if(!useJK50) {
					getGame().setJoker(new JokerFiftyFifty());
					useJK50=true;
					btnJoker50.getStyleClass().add("used");
				}
			});
			btnJoker50.setMinWidth(jokerSizeWidth);
			btnJoker50.setMinHeight(jokerSizeHeight);
			btnJoker50.setWrapText(true);
			btnJoker50.setTextAlignment(TextAlignment.CENTER);
		}
		return btnJoker50;
	}
	
	public Button getBtnLeave() {
		if(btnLeave==null) {
			btnLeave=new Button("Surrender");
			btnLeave.setTextAlignment(TextAlignment.CENTER);
			btnLeave.setWrapText(true);
			btnLeave.setMaxWidth(150);
			btnLeave.getStyleClass().clear();
			btnLeave.getStyleClass().add("record-sales");
			btnLeave.setOnAction(e->{
				getGame().setEtat(State.WIN);
			});
		}
		return btnLeave;
	}
	public int getCompteurTime() {
		return compteurTime=31;
	}
	public GameObservable getGame() {
		if(game==null) {
			game=new GameObservable();
			game.ajouter(this);
			actualise(game);
		}
		return game;
	}
	public Group getGrpButton() {
		if(grpButton==null) {
			grpButton=new Group();
			VBox vbstatement = new VBox(5);
			VBox vChoice1=new VBox(5);
			VBox vChoice2=new VBox(5);
			HBox hChoice1 = new HBox(5);
			vChoice1.getChildren().addAll(getListChoix().get(0),getListChoix().get(1));
			vChoice2.getChildren().addAll(getListChoix().get(2),getListChoix().get(3));
			hChoice1.getChildren().addAll(vChoice1,vChoice2);
			hChoice1.setAlignment(Pos.CENTER);
			vbstatement.getChildren().addAll(getLblStatement(),getLblCorrectAnswer(),hChoice1);
			vbstatement.setTranslateY(90);
			for(Button bt:getListChoix()) {
				bt.setMaxWidth(250);
				bt.setMinWidth(200);
				bt.setMinHeight(170);
				bt.setMaxHeight(200);
				bt.setWrapText(true);
				bt.setTextAlignment(TextAlignment.CENTER);
			}
			
			grpButton.getChildren().addAll(vbstatement,getTimer());
			getLblCorrectAnswer().setTranslateY(50.);
			getLblCorrectAnswer().setMaxWidth(250);
			getLblCorrectAnswer().setMinWidth(125);
			getLblCorrectAnswer().setWrapText(true);
			getLblCorrectAnswer().setTextAlignment(TextAlignment.CENTER);
			getLblStatement().setWrapText(true);
			getLblStatement().setMaxWidth(500);
			getLblStatement().setMinWidth(500);
			getLblStatement().setTextAlignment(TextAlignment.CENTER);
		}
		return grpButton; 
	}
	public Label getLblCorrectAnswer() {
		if(lblCorrectAnswer==null) {
			lblCorrectAnswer=new Label();
			lblCorrectAnswer.setVisible(false);
		}
		return lblCorrectAnswer;
	}
	public Label getLblHelpFriend() {
		if(lblHelpFriend==null) {
			lblHelpFriend=new Label();
			lblHelpFriend.setVisible(false);
			lblHelpFriend.setWrapText(true);
			lblHelpFriend.setMaxWidth(150);
		}
		return lblHelpFriend;
	}
	public Label getLblState() {
		if(lblState==null) {
			lblState=new Label();
		}
		return lblState;
	}
	public Label getLblStatement() {
		if(lblStatement==null) {
			lblStatement=new Label("Empty");
			lblStatement.setMaxWidth(250);
			lblStatement.setWrapText(true);
			lblStatement.setFont(Font.font("Verdana",FontWeight.BOLD,13));
			lblStatement.getStyleClass().add("statement");
			lblStatement.setPadding(new Insets(2));
		}
		return lblStatement;
	}
	public List<Label> getListAidePublic() {
		if(listAidePublic==null) {
			listAidePublic=new ArrayList<>();
			for(int i=0;i<4;i++) {
				listAidePublic.add(new Label());
			}
		}
		return listAidePublic;
	}
	public List<Button> getListChoix() {
		if(listChoix==null) {
			listChoix=new ArrayList<>();
			for(int i=0;i<4;i++) {
				Button b=new Button();
				b.getStyleClass().add("record-sales");
				b.setStyle("-fx-border-color: none;");
				listChoix.add(b);
			}
		}
		for(Button bt:listChoix) {
			bt.setWrapText(true);
			bt.setTextAlignment(TextAlignment.CENTER);
			bt.setOnAction(e->{
				if(bt.getId()=="")
					return;
				if(bt.getStyle().equals("-fx-border-color: none;")) {
					for(Button b:listChoix) {
						b.setStyle("-fx-border-color: none;");
					}
					bt.setStyle("-fx-border-color: blue;-fx-border-width: 2;");
				}
				else {
					getGame().setChoix(bt.getId());
					bt.setStyle("-fx-border-color: none;");
				}
			});
		}
		return listChoix;
	}
	public List<Label> getListPalier() {
		if(listPalier==null) {
			listPalier=new ArrayList<>();
			for(Palier p:Palier.values()) {
				listPalier.add(new Label(p.toString()));
			}
			Collections.reverse(listPalier);
		}
		return listPalier; 
	}
	public VBox getVbPalier() {
		if(vbPalier==null) {
			vbPalier=new VBox(16);
			vbPalier.setPadding(new Insets(10));
			vbPalier.setStyle("-fx-background-color: #F4D06F;-fx-background-radius: 5px;-fx-background-opacity:60%;-fx-border-color: #5D5478;-fx-border-width:2px;-fx-border-radius: 5px;");
			int i=0;
			for(Label l:getListPalier()) {
				vbPalier.getChildren().add(l);
				l.setAlignment(Pos.CENTER);
				l.setMaxWidth(150);
				l.setTextAlignment(TextAlignment.CENTER);
				//l.setStyle("-fx-text-fill: white;");
				if(i==0 || i==5 || i==10)
					l.setStyle("-fx-text-fill: red;");
				i++;
			}
			//vbPalier.setMinHeight(600);
		}
		return vbPalier;
	}
	public Label getTimer() {
		if(timer==null) {
			timer=new Label(getCompteurTime()+"");
			timer.setFont(Font.font("Verdana",FontWeight.BOLD,25));
			
			Thread th=new Thread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					while(!Thread.interrupted()) {
						Platform.runLater(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								setCompteurTime();
							}
						});
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			});
			th.setDaemon(true);
			th.start();
		}
		return timer;
	}
	public PlayObserver(){
		this.getStylesheets().add(Menu.class.getResource("Buttons.css").toExternalForm());
		this.setStyle("-fx-background-color:FFF8F1");
	}
	public void reload() {
		getGame().retirer(this);
		setGame(new GameObservable());
		getGame().ajouter(this);
		getLblStatement().setVisible(true);
		getLblState().setText("");
		getLblState().setStyle("-fx-background-color: none;");
		getTimer().setVisible(true);
		getJokers().setVisible(true);
		int i=0;
		for(Label lb:getListPalier()) {
			lb.setStyle("-fx-background-color: none;");
			if(i==0 || i==5 || i==10)
				lb.setStyle("-fx-text-fill: red;");
			i++;
		}
		getListPalier().get(getListPalier().size()-1).setStyle("-fx-background-color: gray;");
		
		setDecrease(false);
		
		for(Button b:getListChoix()) {
			b.setVisible(true);
		}
		getBtnJoker50().setTextAlignment(TextAlignment.CENTER);
		getBtnJoker50().getStyleClass().clear();
		getBtnJoker50().getStyleClass().add("shiny-orange");
		getBtnJokerAmis().setTextAlignment(TextAlignment.CENTER);
		getBtnJokerAmis().getStyleClass().clear();
		getBtnJokerAmis().getStyleClass().add("shiny-orange");
		getBtnJokerPublic().setTextAlignment(TextAlignment.CENTER);
		getBtnJokerPublic().getStyleClass().clear();
		getBtnJokerPublic().getStyleClass().add("shiny-orange");
		getBtnJokerTimer().setTextAlignment(TextAlignment.CENTER);
		getBtnJokerTimer().getStyleClass().clear();
		getBtnJokerTimer().getStyleClass().add("shiny-orange");
		
		useJK50=false;
		useJKAmis=false;
		useJKTimer=false;
		useJKPublic=false;
		getLblCorrectAnswer().setVisible(false);
	}
	
	public HBox getJokers(){
		if(jokerBox == null) {
			this.jokerBox = new HBox(2);
			this.jokerBox.getChildren().addAll(getBtnJoker50(),getBtnJokerAmis(),getBtnJokerPublic(),getBtnJokerTimer(),getLblHelpFriend());
		}
		return jokerBox;
	}
	
	public void set() {
		getGame();
		setLabelListPalier(getGame().getPalier());
		VBox centerBox = new VBox();
		centerBox.getChildren().addAll(getTimer(),getJokers(),getGrpButton());
		this.setMargin(centerBox, new Insets(2,5,2,100));
		centerBox.setMargin(getGrpButton(), new Insets(5,5,5,55));
		centerBox.setMargin(getTimer(), new Insets(10, 0, 10, 220));
		centerBox.setMargin(getJokers(), new Insets(10, 0, 10, 0));
		
		
		setCenter(centerBox);
		setRight(getVbPalier());
		setBottom(getLblState());
		VBox vb=new VBox(8);
		useJK50=false;
		useJKAmis=false;
		useJKTimer=false;
		useJKPublic=false;
		vb.getChildren().addAll(getBtnBack(),getBtnLeave());
		setLeft(vb);
		vb.setTranslateX(5);
		vb.setTranslateY(5);
		getTimer().setTranslateX(65);
		
	}
	
	public void setCompteurTime() {
		compteurTime-=decrease;
		
		if(compteurTime<=0) {
			if(compteurTime==0)
				getGame().setChoix(null);
			getTimer().setText(null);
			getTimer().setStyle("-fx-border-color: #5D5478;-fx-border-width:6px;-fx-border-radius: 30%;");
			
		}
		else if(compteurTime<=10){
			getTimer().setText(""+compteurTime);
			getTimer().setStyle("-fx-border-color: red;-fx-border-width:6px;-fx-border-radius: 30%;");
			
		}
		else {
			getTimer().setText(""+compteurTime);
			getTimer().setStyle("-fx-border-color: #5D5478;-fx-border-width:6px;-fx-border-radius: 30%;");
		}
	}
	public void setDecrease(Boolean i) {
		if(i)
			decrease=1;
		else
			decrease=0;
	}
	public void setGame(GameObservable g) {
		this.game=g;
		if(!getLblStatement().getText().equals(game.getStatement()))
			getCompteurTime();
		getLblStatement().setText(g.getStatement());
		setLabelListPalier(getGame().getPalier());
		Boolean check=false;
		String[] st= {"A","B","C","D"};
		try {
			if(!useJKAmis && game.getAnswerFriend()!=null) {
				getLblHelpFriend().setText(game.getAnswerFriend());
				getLblHelpFriend().setVisible(true);
				useJKAmis=true;
			}
			else {
				getLblHelpFriend().setVisible(false);
			}
		}
		catch(NullPointerException e) {
			getLblHelpFriend().setVisible(false);
		}
		for(int i=0;i<4;i++) {
			if(i>=game.getChoises().size()) {
				if(check)
					useJKPublic=true;
				getListChoix().get(i).setVisible(false);
			}
			else {
				getListChoix().get(i).setVisible(true);
				getListChoix().get(i).setId(game.getChoises().get(i));
				try {
					if(!useJKPublic) {
						getListChoix().get(i).setText(st[i]+"\n"+game.getListHelpPublic().get(i)+"% : "+game.getChoises().get(i));
						check=true;
						if(i==3)
							useJKPublic=true;
					}
					else {
						getListChoix().get(i).setText(st[i]+"\n"+game.getChoises().get(i));
					}
				}
				catch(NullPointerException e) {
					getListChoix().get(i).setText(st[i]+"\n"+game.getChoises().get(i));
				}
			}
		}
	}
	public void setLabelListPalier(Palier p) {
		for(Label lb:getListPalier()) {
			if(lb.getText().equals(p.toString()))
				lb.setStyle("-fx-background-color: gray;");
		}
	}
}
