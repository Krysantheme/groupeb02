package view;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import modele.Round;

/**
 * Represents a generic object editor.
 * @author Ahmad Mohamad
 *
 * @param <T> The type of the object we are editing.
 */
public class GenericObjectEditor<T> extends Dialog<T>
{
	private GridPane pane;
	/**
	 * Represents the class of the object being edited.
	 */
	private Class<T> tClass;
	private Label[] labels;
	/**
	 * Represents an Array of the parameters types of the class of the object being edited.
	 */
	private Class<?>[] paramsType;
	/**
	 * Represents an Array of the new parameters values of the object being edited.
	 */
	private Object[] paramsValue;
	/**
	 * Represents the number of fields of the class of the object being edited.
	 */
	private int numberOfFields;
	/**
	 * Represents the object being edited.
	 */
	private T object;

	/**
	 * Creates a generic object editor.
	 * @param tClass The class of the object being edited.
	 * @param t The object being edited
	 */
	@SuppressWarnings("unchecked")
	public GenericObjectEditor(Class<T> tClass, T t)
	{
		//Creates the visual part of the object editor.
		setWidth(400);
		setHeight(300);
		object = t;
		this.tClass = tClass;
		this.getDialogPane().setContent(getGridPane());
		setTitle(tClass.getSimpleName()+" Editor");

		ButtonType btOk = new ButtonType("Apply", ButtonData.OK_DONE);
		ButtonType btCc = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

		getDialogPane().getButtonTypes().addAll(btOk,btCc);

		//Upon pressing the apply Button, creates a new instance of the Class T and returns it to update the current object
		//Or add a new one.
		setResultConverter(dialogButton -> 
		{
			if(dialogButton == btOk)
			{
				//Initialises the array used to store the parameters values of the object.
				paramsValue = new Object[numberOfFields];
				int i = 0;
				//Then populates the array with the values from the different boxes.
				for (Node child : pane.getChildren()) 
				{
					System.out.println(child.getClass());
					if(child instanceof TextField)
					{
						String val = ((TextField)child).getText();

						setObjectFromString(val,i);

						i++;
					}
					else if(child instanceof ComboBox)
					{
						String val = "";
						try
						{
							val = ((ComboBox<String>)child).getValue();
						}
						catch(ClassCastException ex)
						{

						}

						setObjectFromString(val,i);

						i++;
					}
					else if(child instanceof VBox)
					{
						VBox vb = (VBox) child;
						Map<String,Boolean> choices = new HashMap();
						HBox hb;
						if(vb.getChildren().get(0) instanceof HBox) {
							try
							{
								for(Node n : vb.getChildren())
								{
									hb = (HBox)n;
									RadioButton b = (RadioButton)hb.getChildren().get(0);
									TextField val = (TextField)hb.getChildren().get(1);
									choices.put(val.getText(),b.isSelected());
								}
								setObjectFromMap(choices, i);
							}
							catch(ClassCastException ex)
							{

							}
								
								i++;
						}
					}
				}
				T instance = null;
				//Create the new instance of the T Class, by getting the constructor using the parameters types array,
				//then calling the constructor with the parameters values.
				try {
					Constructor<T> constructor = tClass.getConstructor(paramsType);
					try {
						instance = constructor.newInstance(paramsValue);
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
							| InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (NoSuchMethodException | SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(instance.toString());
				return instance;
			}

			return null;
		});
	}
	
	/**
	 * Sets the parameter value at the given index by casting the given value.
	 * @param s The Map representation of the value.
	 * @param i The index of the parameter in the array.
	 */
	private void setObjectFromMap(Map<String,Boolean> h, int i)
	{
		try
		{
			paramsValue[i] = h;
		}
		catch (Exception e) 
		{
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Type error");
			alert.setContentText("The field "+labels[i].getText()+" have the wrong type of value\nShould be "+paramsType[i]);
			alert.showAndWait();
		}

	}
	
	
	/**
	 * Sets the parameter value at the given index by casting the given value.
	 * @param s The string representation of the value.
	 * @param i The index of the parameter in the array.
	 */
	private void setObjectFromString(String s, int i)
	{
		System.out.println(s);
		//For each parameter, cast it by looking to the corresponding type in the parameters types array,
		//then add it to the parameters values array.
		if(paramsType[i] == int.class)
		{
			try
			{
				paramsValue[i] = Integer.parseInt(s);
			}
			catch (Exception e) 
			{
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Type error");
				alert.setContentText("The field "+labels[i].getText()+" have the wrong type of value\nShould be "+paramsType[i]);
				alert.showAndWait();
			}
		}
		else if(paramsType[i] == boolean.class)
		{
			try
			{
				paramsValue[i] = Boolean.parseBoolean(s);
			}
			catch (Exception e) 
			{
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Type error");
				alert.setContentText("The field "+labels[i].getText()+" have the wrong type of value\nShould be "+paramsType[i]);
				alert.showAndWait();
			}
		}
		else if(paramsType[i] == Round.class)
		{
			try
			{
				switch(s) 
				{
					case "Easy" : 
						paramsValue[i] = Round.FIRST_ROUND;
						break;
					case "Medium" : 
						paramsValue[i] = Round.SECOND_ROUND;
						break;
					case "Hard" : 
						paramsValue[i] = Round.LAST_ROUND;
						break;
				}
				System.out.println(paramsValue[i]);
			}
			catch(Exception e)
			{
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Type error");
				alert.setContentText("The field "+labels[i].getText()+" have the wrong type of value\nShould be "+paramsType[i]);
				alert.showAndWait();
			}
		}
		else
		{
			paramsValue[i] = s;
		}
	}

	/**
	 * Gets the GridPane containing all the labels and boxes representing the parameters of the class of the object being edited.
	 * @return The GridPane containing all the labels and boxes representing the parameters of the class of the object being edited.
	 * @see GridPane
	 */
	private GridPane getGridPane()
	{
		//If the GridPane doesn't exist yet, creates it.
		if(pane == null)
		{
			pane = new GridPane();
			int i = 0;
			//Count all the non-static non-transient fields in the class of the object being edited.
			for (Field field : tClass.getDeclaredFields()) 
			{
				if(!Modifier.isStatic(field.getModifiers()) &&
						!Modifier.isTransient(field.getModifiers()))
				{
					i++;
				}
			}
			numberOfFields = i;
			paramsType = new Class[i];
			labels = new Label[i];
			i = 0;
			//For each non-static non-transient fields in the class of the object being edited,
			//add a label and a box of the needed type.
			for (Field field : tClass.getDeclaredFields()) 
			{
				if(!Modifier.isStatic(field.getModifiers()) &&
						!Modifier.isTransient(field.getModifiers()))
				{
					paramsType[i] = field.getType();
					Label label = new Label(field.getName());
					pane.add(label, 0, i);

					//If the object isn't null (which mean we are editing and not adding),
					//Gets the getter of the current field to not break the encapsulation.
					//And then fill the box of the field with the field's value of the object.
					if(object != null)
					{
						Method getter = null;
						try 
						{							
							for(Method m : object.getClass().getDeclaredMethods())
							{								
								if(m.getName().equalsIgnoreCase("get"+field.getName()))
								{
									getter = m;
								}
							}
						}
						catch(Exception ex)
						{
							//Catch general exception( either that or 50 try/catch )
							ex.printStackTrace();
						}
						try 
						{
							Object val = null;
							try {
								val = getter.invoke(object);
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							System.out.println(field.getType() + " " + val.toString());
							if(field.getType().equals(String.class) || field.getType().equals(int.class))
							{
								TextField tfield = new TextField();
								tfield.setText(val.toString());
								pane.add(tfield, 1, i);
							}
							else if(field.getType().equals(boolean.class))
							{
								ComboBox<String> tfield = new ComboBox<String>();
								boolean bool = (boolean)val;
								tfield.getItems().add("true");
								tfield.getItems().add("false");
								tfield.setValue(bool == true ? "true" : "false");
								pane.add(tfield, 1, i);
							}
							else if(field.getType().equals(Round.class))
							{
								Round loc = (Round) val;
								ComboBox cb = new ComboBox();
								List<Round> lcs = Arrays.asList(Round.values());
								
								for(Round l : lcs) {
									if(loc.equals(l)) 
									{
										cb.getItems().add(l);
										cb.getSelectionModel().select(loc);
									}else {
										cb.getItems().add(l);
									}
								}
								pane.add(cb, 1, i);
							}
							else if(field.getType().toString().contains("Map"))
							{
								Map<String,Boolean> map = (Map<String,Boolean>) val;
								List<HBox> hlist = new ArrayList<HBox>();
								
								VBox choices = new VBox(2);
								ToggleGroup rg = new ToggleGroup();
								    for (Map.Entry<String, Boolean> entry : map.entrySet()) 
								    {
							    		HBox newEntry = new HBox(2);
										RadioButton rb = new RadioButton();
								    	if(entry.getValue().equals(Boolean.TRUE)) 
								    	{
											rb.setSelected(true);
											rb.setToggleGroup(rg);
											newEntry.getChildren().addAll(rb,new TextField(entry.getKey()));
								    	}
								    	else
								    	{
								    		rb.setSelected(false);
											rb.setToggleGroup(rg);
											newEntry.getChildren().addAll(rb,new TextField(entry.getKey()));
								    	}
								    	choices.getChildren().add(newEntry);
								    	
								    }
								    pane.add(choices, 1, i);
								    
							}
							
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					//If we are adding a new object, just create the label and box of the field.
					else
					{
						if(field.getType().equals(String.class) || field.getType().equals(int.class))
						{
							TextField tfield = new TextField();
							pane.add(tfield, 1, i);
						}
						else if(field.getType().equals(boolean.class))
						{
							ComboBox<String> tfield = new ComboBox<String>();
							tfield.getItems().add("Yes");
							tfield.getItems().add("No");
							pane.add(tfield, 1, i);
						}
					}
					labels[i] = label;
					i++;
				}
			}
		}
		return pane;
	}
}
