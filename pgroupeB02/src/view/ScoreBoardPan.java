package view;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import modele.Score;
import modele.ScoreBoard;

public class ScoreBoardPan extends BorderPane {
	private Button btnBack;
	private Label labelFirst,labelSecond,labelThird;
	private List<HBox> listLabel;
	private ScoreBoard score;
	private BackgroundImage myBI;

	public ScoreBoardPan() {
		setTop(getBtnBack());
		BorderPane.setAlignment(getBtnBack(),Pos.TOP_LEFT);
		this.getStylesheets().add(ScoreBoardPan.class.getResource("Buttons.css").toExternalForm());
		//this.setStyle("-fx-background-color:FFF8F1");
		myBI= new BackgroundImage(new Image(getClass().getResourceAsStream("/resources/wallpapers/leaf-wallpaper-0.jpg")),
		        BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT,
		          BackgroundSize.DEFAULT);
		
		this.setBackground(new Background(myBI));
		VBox vb=new VBox(5);
		vb.getChildren().addAll(getListLabel());
		setRight(vb);
		Group grp=new Group();
		grp.getChildren().addAll(getLabelFirst(),getLabelSecond(),getLabelThird());
		grp.setTranslateY(-50);
		setCenter(grp);
		
		
	}

	public Button getBtnBack() {
		if(btnBack==null) {
			btnBack=new Button("Back to menu");
			btnBack.getStyleClass().add("record-sales");
			btnBack.setPadding(new Insets(5,5,5,5));
			btnBack.setMinWidth(150);
			btnBack.setMinHeight(40);
			btnBack.setTranslateX(5.);
			btnBack.setTranslateY(5.);
		}
		return btnBack;
	}

	public Label getLabelFirst() {
		if(labelFirst==null) {
			labelFirst=new Label(" 1\n "+getScore().get(0).getNickName()+" : "+getScore().get(0).getMoney()+"� ");
			labelFirst.setMinWidth(250.);
			labelFirst.setWrapText(true);
			labelFirst.setTranslateY(-150);
			labelFirst.setPadding(new Insets(10));
			labelFirst.setTranslateX(50);
			labelFirst.setTextAlignment(TextAlignment.CENTER);
			labelFirst.setStyle("-fx-font-size: 40;-fx-background-color: #F4D06F;-fx-background-radius: 5px;-fx-background-opacity:60%;-fx-border-color: #5D5478;-fx-border-width:2px;-fx-border-radius: 5px;");

		}
		return labelFirst;
	}

	public Label getLabelSecond() {
		if(labelSecond==null) {
			labelSecond=new Label(" 2\n "+getScore().get(1).getNickName()+" : "+getScore().get(1).getMoney()+"� ");
			labelSecond.setMinWidth(250.);
			labelSecond.setWrapText(true);
			labelSecond.setPadding(new Insets(10));
			labelSecond.setTranslateX(-150);
			labelSecond.setTextAlignment(TextAlignment.CENTER);
			labelSecond.setStyle("-fx-font-size: 40;-fx-background-color: #F4D06F;-fx-background-radius: 5px;-fx-background-opacity:60%;-fx-border-color: #5D5478;-fx-border-width:2px;-fx-border-radius: 5px;");

		}
		return labelSecond;
	}

	public Label getLabelThird() {
		if(labelThird==null) {
			labelThird=new Label(" 3\n "+getScore().get(2).getNickName()+" : "+getScore().get(2).getMoney()+"� ");
			labelThird.setMinWidth(250.);
			labelThird.setPadding(new Insets(10));
			labelThird.setWrapText(true);
			labelThird.setTranslateX(250);
			labelThird.setTextAlignment(TextAlignment.CENTER);
			labelThird.setStyle("-fx-font-size: 40;-fx-background-color: #F4D06F;-fx-background-radius: 5px;-fx-background-opacity:60%;-fx-border-color: #5D5478;-fx-border-width:2px;-fx-border-radius: 5px;");

		}
		return labelThird;
	}

	public List<HBox> getListLabel() {
		if(listLabel==null) {
			listLabel=new ArrayList<>();
			for(int i=0;i<10;i++) {
				int j=i+1;
				Label num=new Label(" "+j);
				num.setMinWidth(40);
				Label ln=new Label(" "+getScore().get(i).getNickName()+" : "+getScore().get(i).getMoney()+"� ");
				ln.setMinWidth(225.);
				ln.setMinHeight(30.);
				num.setTextAlignment(TextAlignment.CENTER);
				ln.setTextAlignment(TextAlignment.CENTER);
				ln.setStyle("-fx-font-size: 25;-fx-background-color: #F4D06F;-fx-background-radius: 5px;-fx-background-opacity:60%;-fx-border-color: #5D5478;-fx-border-width:2px;-fx-border-radius: 5px;");
				num.setStyle("-fx-font-weight: bold;-fx-font-size: 25;-fx-background-color: #F4D06F;-fx-background-radius: 5px;-fx-background-opacity:60%;-fx-border-color: #5D5478;-fx-border-width:2px;-fx-border-radius: 5px;");

				HBox hb=new HBox(3);
				hb.getChildren().addAll(num,ln);
				listLabel.add(hb);
			}
		}
		return listLabel;
	}

	public ScoreBoard getScore() {
		if(score==null) {
			score=new ScoreBoard();
			score.loadScoreBoard("saveScore");
			score.sort();
		}
		return score;
	}
	public void reload() {
		List<HBox> list=new ArrayList<>();
		for(int i=0;i<10;i++) {
			int j=i+1;
			Label num=new Label(""+j);
			num.setMinWidth(40);
			num.setTextAlignment(TextAlignment.CENTER);
			Label ln=new Label(" "+getScore().get(i).getNickName()+" : "+getScore().get(i).getMoney()+"� ");
			ln.setMinWidth(225.);
			ln.setMinHeight(30.);
			ln.setTextAlignment(TextAlignment.CENTER);
			ln.setStyle("-fx-font-size: 25;-fx-background-color: #F4D06F;-fx-background-radius: 5px;-fx-background-opacity:60%;-fx-border-color: #5D5478;-fx-border-width:2px;-fx-border-radius: 5px;");
			num.setStyle("-fx-font-weight: bold;-fx-font-size: 25;-fx-background-color: #F4D06F;-fx-background-radius: 5px;-fx-background-opacity:60%;-fx-border-color: #5D5478;-fx-border-width:2px;-fx-border-radius: 5px;");

			HBox hb=new HBox(3);
			hb.getChildren().addAll(num,ln);
			list.add(hb);
		}
		listLabel=list;
		getLabelFirst().setText(" 1\n "+getScore().get(0).getNickName()+" : "+getScore().get(0).getMoney()+"� ");
		getLabelSecond().setText(" 2\n "+getScore().get(1).getNickName()+" : "+getScore().get(1).getMoney()+"� ");
		getLabelThird().setText(" 3\n "+getScore().get(2).getNickName()+" : "+getScore().get(2).getMoney()+"� ");
		VBox b=new VBox(5);
		b.getChildren().addAll(listLabel);
		this.setRight(b);
		
	}
	
	
}
