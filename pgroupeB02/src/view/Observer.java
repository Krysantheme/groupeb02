package view;

import modele.Observable;

public interface Observer {
	public void actualise(Observable o);
}
