package view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import application.Main;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;
import modele.Account;
import util.AccountsManager;
import view.DisplayLoginOptions;

/**
 * This class if the graphical view of the menu
 *
 */

public class Menu extends BorderPane{
	private Label lblTitle,dateTime,currentPlayerLabel;
	
	private Button btnPlay,btnScore,btnAdminMenu,connectButton, inscriptionButton, disconnectButton;
	private String playerName="invit�";
	private BackgroundImage myBI;
	
	@SuppressWarnings("static-access")
	public Menu() {
		
		this.setPadding(new Insets(10));
		this.getStylesheets().add(Menu.class.getResource("Buttons.css").toExternalForm());
		myBI= new BackgroundImage(new Image(getClass().getResourceAsStream("/resources/wallpapers/leaf-wallpaper-0.jpg")),
		        BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT,
		          BackgroundSize.DEFAULT);
		
		this.setBackground(new Background(myBI));
		VBox vb = new VBox(4);
		vb.getChildren().addAll(
				getLblTitle(),
				getBtnPlay(), 
				getBtnScore(),
				getBtnAdminMenu());
		vb.setAlignment(Pos.CENTER);
		vb.setSpacing(10);
		btnPlay.setPrefSize(200, 35);
		btnScore.setPrefSize(200, 35);
		btnAdminMenu.setPrefSize(200, 35);
		getBtnAdminMenu().setVisible(false);
		
		this.setCenter(vb);
		
		VBox accountBox = new VBox();
		HBox accountNameBox = new HBox(4);
		accountNameBox.getChildren().add(getCurrentPlayerLabel());
		HBox accountButtonBox = new HBox(4);
		accountButtonBox.getChildren().add(getConnectButton());
		accountButtonBox.getChildren().add(getInscriptionButton());
		accountButtonBox.setMargin(getCurrentPlayerLabel(), new Insets(5,5,5,0));
		//accountButtonBox.getChildren().add(getTimeLabel());		
		StackPane accountButtonPane = new StackPane();
		accountButtonPane.getChildren().add(accountButtonBox);
		accountButtonPane.getChildren().add(getDisconnectButton());
		accountButtonPane.setAlignment(Pos.TOP_LEFT);
		getDisconnectButton().setVisible(false);
		accountBox.getChildren().addAll(accountNameBox,accountButtonPane);
		this.setTop(accountBox);
	}
	
	public String getPlayerName() {
		return playerName;
	}

	/**
	 * Return the title in a label
	 * @return an instance of label, that means the title of the game in the menu
	 */
	public Label getLblTitle() {
		if(lblTitle==null) {
			lblTitle=new Label("Millionaire");
			lblTitle.setId("Title");
			
			try {
				lblTitle.setFont(Font.loadFont(new FileInputStream(new File("file/fonts/Airstream.ttf")), 20));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			lblTitle.getStyleClass().add("main-title");
		}
		return lblTitle;
	}
	
	/**
	 * returns the placeholder that will contain the current player's name if he's connected.
	 */
	public Label getCurrentPlayerLabel()
	{
		if(currentPlayerLabel == null)
		{
			currentPlayerLabel = new Label();
			playerName="invit�";
			currentPlayerLabel.setText("Current player : none");
			currentPlayerLabel.getStyleClass().add("outline");
			currentPlayerLabel.getStyleClass().add("green");
			currentPlayerLabel.setPadding(new Insets(5,5,5,5));
			currentPlayerLabel.setTextFill(Color.RED);
		}

		return currentPlayerLabel; 
	}
	
	/**
	 * Disconnect the player
	 * @return
	 */
	public Button getDisconnectButton()
	{
		if(disconnectButton == null)
		{
			disconnectButton = new Button();
			disconnectButton.setText("Disconnect");
			disconnectButton.getStyleClass().add("record-sales");
			disconnectButton.setPadding(new Insets(5,5,5,5));
			disconnectButton.setMinWidth(150);
			disconnectButton.setMinHeight(40);
			disconnectButton.setOnAction(e ->
			{
				getBtnAdminMenu().setVisible(false);
				getBtnPlay().setVisible(true);
				getDisconnectButton().setVisible(false);
				getConnectButton().setVisible(true);
				getInscriptionButton().setVisible(true);
				getCurrentPlayerLabel().setText("Current player : none");
				playerName = "invit�";
			});
		}

		return disconnectButton; 
	}
	
	/**
	 * Show the connection dialog allowing to connect by providing the right nickname/password pair.
	 * @return
	 */
	public Button getConnectButton()
	{
		if(connectButton == null)
		{
			connectButton = new Button();
			connectButton.setText("Log in");
			connectButton.getStyleClass().clear();
			connectButton.getStyleClass().add("record-sales");
			connectButton.setPadding(new Insets(5,5,5,5));
			connectButton.setMinWidth(150);
			connectButton.setMinHeight(40);
			connectButton.setOnAction(e ->
			{
				Optional<Account> result = DisplayLoginOptions.launchDisplayConnection();

				if(result.isPresent())
				{
					Account account = result.get();
					playerName = account.getNickname();
					getCurrentPlayerLabel().setText("Current player : "+ playerName);
					currentPlayerLabel.setTextFill(Color.GREEN);
					getDisconnectButton().setVisible(true);
					getConnectButton().setVisible(false);
					getInscriptionButton().setVisible(false);

					if(account.getIsAdministrator())
					{
						getBtnPlay().setVisible(false);
						getBtnAdminMenu().setVisible(true);
					}
				}
			});
		}
		return connectButton; 
	}
	
	
	/**
	 * Show the inscription dialog allowing to create a new account
	 * @return
	 */
		public Button getInscriptionButton()
		{
			if(inscriptionButton == null)
			{
				inscriptionButton = new Button();
				inscriptionButton.setText("Create a new account");
				inscriptionButton.getStyleClass().clear();
				inscriptionButton.getStyleClass().add("record-sales");
				inscriptionButton.setPadding(new Insets(5,5,5,5));
				inscriptionButton.setMinWidth(150);
				inscriptionButton.setMinHeight(40);
				inscriptionButton.setOnAction(e -> 
				{
					Optional<Account> result = DisplayLoginOptions.launchDisplayInscription();

					if(result.isPresent())
					{							
						Account player = result.get();

						AccountsManager.saveAccounts();
						getCurrentPlayerLabel().setText("Current player : "+player.getNickname());
						getDisconnectButton().setVisible(true);
						getConnectButton().setVisible(false);
						getInscriptionButton().setVisible(false);
					}
				});
			}

			return inscriptionButton; 
		}
	
	/**
	 * Return the button of play
	 * @return an instance of button, that means the name of the button to access in the panel of play
	 */
	public Button getBtnPlay() {
		if(btnPlay==null) {
			btnPlay=new Button("Play");
			btnPlay.setId("Play");
			btnPlay.getStyleClass().clear(); 
			btnPlay.getStyleClass().add("record-sales");
			
			
		}
		return btnPlay;
	}
	
	/**
	 * Return the button of score
	 * @return an instance of button, that means the name of the button to access in the panel of scoreboard
	 */
	public Button getBtnScore() {
		if(btnScore==null) {
			btnScore=new Button("Scoreboard");
			btnScore.setId("Score");
			btnScore.getStyleClass().clear();
			btnScore.getStyleClass().add("record-sales");
		}
		return btnScore;
	}	

	/**
	 * Return the button of admin menu
	 * @return an instance of button, that means the name of the button to access in the panel of admin menu
	 */
	public Button getBtnAdminMenu() {
		if(btnAdminMenu==null) {
			btnAdminMenu=new Button("Admin Menu");
			btnAdminMenu.setId("AdminMenu");
			btnAdminMenu.getStyleClass().clear();
			btnAdminMenu.getStyleClass().add("record-sales");
		}
		return btnAdminMenu;
	}
	 
	private Label getTimeLabel() {
		
		if(this.dateTime == null) {
		dateTime = new Label();
	    Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
	        dateTime.setText(" Bonjour " + playerName + " nous sommes le " + LocalDateTime.now().format(formatter));
	    }), new KeyFrame(Duration.seconds(1)));
	    clock.setCycleCount(Animation.INDEFINITE);
	    clock.play();
		}
		return dateTime;
	}
	

}
