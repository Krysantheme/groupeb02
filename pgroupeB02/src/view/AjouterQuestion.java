package view;

import java.util.ArrayList;
import java.util.List;

import exceptions.AlreadyTrueException;
import exceptions.DoublonException;
import exceptions.StringEmptyException;
import exceptions.TooManyChoicesException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import modele.Deck;
import modele.Question;
import modele.Round;

public class AjouterQuestion extends BorderPane {
	private List<RadioButton> rdButton;
	private List<TextField> textChoix;
	private ToggleGroup toggleChoix;
	private TextField txtQuestion,txtAutor;
	private Button btnSave,btnConfirme;
	private Deck deck;
	private ComboBox<Round> cbRound;
	private Label lbError;

	public AjouterQuestion() {
		deck=Deck.getInstance();
		VBox vb=new VBox(4);
		vb.setPadding(new Insets(5,5,5,5));
		HBox hb=new HBox(2);
		hb.getChildren().addAll(new Label("Question"),getTxtQuestion());
		for(RadioButton r:getRdButton()) {
			hb=new HBox(2);
			hb.getChildren().addAll(getTextChoix().get(getRdButton().indexOf(r)),r);
			hb.setPadding(new Insets(3,3,3,3));
			vb.getChildren().add(hb);
		}
		vb.getChildren().add(getCbRound());
		hb=new HBox(2);
		hb.getChildren().addAll(new Label("Author"),getTxtAutor());
		vb.getChildren().add(hb);
		this.setCenter(vb);
		hb=new HBox(2);
		hb.getChildren().addAll(getLbError(null),getBtnSave(),getBtnConfirme());
		this.setBottom(hb);
		BorderPane.setAlignment(hb, Pos.BOTTOM_RIGHT);
	}
	public List<RadioButton> getRdButton() {
		
		if(rdButton==null) {
			rdButton=new ArrayList<>();
			for(int i=0;i<4;i++) {
				RadioButton r=new RadioButton("true");
				r.setToggleGroup(getToggleChoix());
				rdButton.add(r);
				if(i==0)r.setSelected(true);
			}
		}
		return rdButton;
	}
	public List<TextField> getTextChoix() {
		if(textChoix==null) {
			textChoix=new ArrayList<>();
			for(int i=1;i<5;i++) {
				TextField txt=new TextField();
				txt.setPromptText("Choix n�"+i);
				textChoix.add(txt);
			}
		}
		return textChoix;
	}
	public ToggleGroup getToggleChoix() {
		if(toggleChoix==null)
			toggleChoix=new ToggleGroup();
		return toggleChoix;
	}
	public TextField getTxtQuestion() {
		if(txtQuestion==null) {
			txtQuestion=new TextField();
			txtQuestion.setPromptText("Question");
		}
		return txtQuestion;
	}
	public TextField getTxtAutor() {
		if(txtAutor==null) {
			txtAutor=new TextField();
			txtAutor.setPromptText("Author");
		}
		return txtAutor;
	}
	public Button getBtnSave() {
		if(btnSave==null) {
			btnSave=new Button("Save");
			btnSave.setOnAction(e->{
				deck.saveDeck("test");
				System.out.println(deck);
			});
		}
		return btnSave;
	}
	public Label getLbError(String error) {
		if(lbError==null) {
			lbError=new Label();
		}
		lbError.setText(error);
		return lbError;
		
	}
	public Button getBtnConfirme() {
		if(btnConfirme==null) {
			btnConfirme=new Button("Confirm");
			btnConfirme.setOnAction(e->{
				if(getTxtAutor().getText().isEmpty()) {
					getTxtAutor().setStyle("-fx-background-color: red;");
					return;
				}else
					getTxtAutor().setStyle("-fx-background-color: none;");
				if(getTxtQuestion().getText().isEmpty()) {
					getTxtQuestion().setStyle("-fx-background-color: red;");
					return;
				}else
					getTxtQuestion().setStyle("-fx-background-color: none;");
				Boolean choix=false;
				for(TextField txt:getTextChoix()) {
					if(txt.getText().isEmpty()) {
						txt.setStyle("-fx-background-color: red;");
						choix=true;
					}
					else
						txt.setStyle("-fx-background-color: none;");
				}
				if(choix)
					return;
				Question q=new Question(getTxtAutor().getText(), getTxtQuestion().getText(), getCbRound().getSelectionModel().getSelectedItem());
				for(TextField txt:getTextChoix()) {
					if(getRdButton().get(getTextChoix().indexOf(txt)).isSelected()) {
						try {
							q.addChoice(txt.getText(), true);
						} catch (AlreadyTrueException | StringEmptyException | TooManyChoicesException e1) {
							// TODO Auto-generated catch block
							getLbError(e1.getMessage());
							e1.printStackTrace();
						}
					}
					try {
						q.addChoice(txt.getText(), false);
					} catch (AlreadyTrueException | StringEmptyException | TooManyChoicesException e1) {
						// TODO Auto-generated catch block
						getLbError(e1.getMessage());
						e1.printStackTrace();
					}
				}
				try {
					deck.add(q);
				} catch (DoublonException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					getLbError(e1.getMessage());
				}
			});
		}
		return btnConfirme;
	}
	public ComboBox<Round> getCbRound() {
		if(cbRound==null) {
			cbRound=new ComboBox<>();
			for(Round e:Round.values()) {
				cbRound.getItems().add(e);
			}
			cbRound.getSelectionModel().select(0);
		}
		return cbRound;
	}
	
}
