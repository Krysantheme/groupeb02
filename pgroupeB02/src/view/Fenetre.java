package view;

import application.Main;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import modele.GameObservable;
import modele.Score;
import modele.State;

public class Fenetre extends StackPane {
	private Menu menu;
	private PlayObserver playPane;
	private ScoreBoardPan scorePane;
	
	public Menu getMenu() {
		if(menu==null) {
			menu=new Menu();
			menu.getBtnPlay().setOnAction(e->{
				if(getPlayPane().getGame().getEtat().equals(State.LOSE)) {
					getPlayPane().reload();
					getPlayPane().getCompteurTime();
					
				}
				if(getPlayPane().getGame().getEtat().equals(State.WIN)) {
					getPlayPane().reload();
					getPlayPane().getCompteurTime();
					
				}
				getPlayPane().set();
				hideAll();
				
				getChildren().get(0).setVisible(true);
				playPane.setDecrease(true);
			
			});
			menu.getBtnAdminMenu().setOnAction(e->{
				Main.getStage().setScene(new Scene(new GenericEditor(),Main.getStageWidth(),Main.getStageHeight()));
			});
			menu.getBtnScore().setOnAction(e->{
				hideAll();
				getChildren().get(2).setVisible(true);
			});
		}
		return menu;
	}
	public ScoreBoardPan getScoreBoardPan() {
		if(scorePane==null) {
			scorePane=new ScoreBoardPan();
			scorePane.getScore().sort();
			scorePane.getBtnBack().setOnAction(e->{
				hideAll();
				this.getChildren().get(1).setVisible(true);
				
			});
		}
		return scorePane;
	}
	public PlayObserver getPlayPane() {
		if(playPane==null) {
			playPane=new PlayObserver();
			playPane.getBtnBack().setOnAction(e->{
				hideAll();
				this.getChildren().get(1).setVisible(true);
				playPane.setDecrease(false);
				if(playPane.getGame().getEtat().equals(State.LOSE) || playPane.getGame().getEtat().equals(State.WIN)) {
					getScoreBoardPan().getScore().addScore(new Score(getMenu().getPlayerName(),playPane.getGame().getMoneySave()));
					getScoreBoardPan().getScore().sort();
					getScoreBoardPan().getScore().saveScoreBoard("saveScore");
					getScoreBoardPan().reload();
				}
			});
		}
		return playPane;
	}
	public void hideAll() {
		for(Node n:getChildren()) {
			n.setVisible(false);
		}
	}
	public Fenetre() {
		getChildren().addAll(getPlayPane(),getMenu(),getScoreBoardPan());
		hideAll();
		getChildren().get(1).setVisible(true);
	}
}
