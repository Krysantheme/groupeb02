package view;

import java.util.List;
import java.util.Optional;

import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import modele.Account;
import modele.Deck;
import modele.Question;
import util.AccountsManager;

/**
 * Represents a generic list editor.
 * @author Ahmad Mohamad
 */
public class GenericEditor extends BorderPane
{
	/**
	 * Represents the current list of objects being edited.
	 */
	ObservableList<Object> observableList;
	/**
	 * Represents the original list of the objects we are editing.
	 */
	List<?> originalList;
	/**
	 * Represents the class of the objects we are editing.
	 */
	Class<?> classe;
	/**
	 * Visually represents the list of objects we are editing.
	 */
	GenericTableView<?> tv;
	Button backButton,applyButton,addButton, resetQuestionsButton;
	Label informationLabel;
	/**
	 * Represents the box used to select the type of objects we want to edit.
	 */
	ComboBox<String> selectionBox;
	VBox vbox, tableViewEditBox;
	HBox tableViewBox, questionHBox,editionBox;

	MediaPlayer mp;
	//String musicPath = "Soundtrack/edittheme.mp3";
	/**
	 * Creates a generic editor.
	 */
	public GenericEditor()
	{
		//Loads memory
		Deck.getInstance().loadQuestions("test");
		//Creates all the visual elements on the screen
		vbox = new VBox();
		vbox.getChildren().add(getInformationLabel());
		vbox.getChildren().add(getSelectionBox());
		getTableViewBox().getChildren().add(getTableViewEditBox(""));
		vbox.getChildren().add(getTableViewBox());
		vbox.getChildren().add(getEditionBox());
		vbox.setAlignment(Pos.CENTER);
		vbox.setSpacing(25);
		setCenter(vbox);
		
		/*String path = getClass().getResource(musicPath).toExternalForm();
		Media media = new Media(path);
		mp = new MediaPlayer(media);
		mp.setCycleCount(MediaPlayer.INDEFINITE);
		mp.play();*/
	}

	/**
	 * Gets the ComboBox used to select the objects we want to edit.
	 * @return A ComboBox used to select the objects we want to edit.
	 * @see ComboBox
	 */
	public ComboBox<String> getSelectionBox()
	{
		//If the box doesn't exist yet, creates it.
		if(selectionBox == null)
		{
			selectionBox = new ComboBox<>();
			//Adds to the box the possible class of objects we can edit.
			selectionBox.getItems().add("Questions");
			selectionBox.getItems().add("Accounts");

			//When we select one of the possible classes, updates the TableView accordingly.
			selectionBox.setOnAction(event ->
			{			
				//Clears the tableViewBox
				getTableViewBox().getChildren().clear();

				//Sets the class and original list, then reset the tableViewBox with the new TableView.
				if(selectionBox.getSelectionModel().getSelectedItem().equals("Questions"))
				{
					classe = Question.class;
					originalList = Deck.getInstance().getQuestions();
					getTableViewBox().getChildren().add(getTableViewEditBox("Questions"));
				}
				else
				{
					classe = Account.class;
					originalList= AccountsManager.getAccounts();
					getTableViewBox().getChildren().add(getTableViewEditBox("Accounts"));
				}
			});
		}

		return selectionBox;
	}

	/**
	 * Gets the VBox containing the TableView and it's Buttons.
	 * @param type Represent the type of objects we want to edit.
	 * @return a VBox containing the TableView and it's Buttons.
	 * @see VBox
	 */
	public VBox getTableViewEditBox(String type)
	{
		tableViewEditBox = new VBox();
		//Centers the elements in the VBox
		tableViewEditBox.setAlignment(Pos.CENTER);

		//Depending on the type of objects, adds to the VBox the corresponding TableView and needed Buttons.
		//Can also returns a VBox with an empty TableView, used when the editor is first launched, as a placeholder.
		//The questions editor need to have an additional reset Button.
		if(type.equals("Questions"))
		{
			tableViewEditBox.getChildren().add(getTableView(Deck.getInstance().getQuestions(),Question.class,true));
			tableViewEditBox.getChildren().add(getQuestionHBox());
		}
		else if(type.equals("Accounts"))
		{
			tableViewEditBox.getChildren().add(getTableView(AccountsManager.getAccounts(),Account.class,true));
			tableViewEditBox.getChildren().add(getAddButton(Account.class));
		}
		else
		{
			TableView<?> dummyTv = new TableView<>();
			dummyTv.setPrefWidth(Main.getStageWidth()*0.95);
			tableViewEditBox.getChildren().add(dummyTv);
		}

		return tableViewEditBox;
	}

	/**
	 * Gets the HBox containing the apply and back Buttons.
	 * @return The HBox containing the apply and back Buttons.
	 * @see HBox
	 */
	public HBox getEditionBox()
	{
		//If the box doesn't exist yet, creates it.
		if(editionBox == null)
		{
			editionBox = new HBox();
			editionBox.getChildren().add(getApplyButton());
			editionBox.getChildren().add(getBackButton());
			editionBox.setAlignment(Pos.CENTER);
			editionBox.setSpacing(75);
		}

		return editionBox;
	}

	/**
	 * Gets the HBox containing the TableView and it's Buttons.
	 * @return The HBox containing the TableView and it's Buttons.
	 * @see HBox
	 */
	public HBox getTableViewBox()
	{
		//If the box doesn't exist yet, creates it.
		if(tableViewBox == null)
		{
			tableViewBox = new HBox();
			tableViewBox.setAlignment(Pos.CENTER);
		}

		return tableViewBox;
	}

	/**
	 * Gets the HBox containing the add and reset questions Buttons.
	 * @return The HBox containing the add and reset questions Buttons.
	 * @see HBox
	 */
	public HBox getQuestionHBox()
	{
		//If the box doesn't exist yet, creates it.
		if(questionHBox == null)
		{
			questionHBox = new HBox();
			questionHBox.setAlignment(Pos.CENTER);
			questionHBox.getChildren().add(getAddButton(Question.class));
		}

		return questionHBox;
	}

	/**
	 * Gets the add Button.
	 * @param classe The class of the objects we are editing.
	 * @param <T> The type of the objects we are editing.
	 * @return The add Button.
	 * @see Button
	 */
	public <T> Button getAddButton(Class<T> classe)
	{
		//Gets the string representation of the class to change the Button text.
		String string = classe == Question.class ? "question" : "account";
		addButton = new Button("Add new "+ string);
		addButton.setAlignment(Pos.CENTER);

		//When the button is pressed, opens the GenericObjectEditor of the current class, and wait for the user to fill it.
		addButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) 
			{
				GenericObjectEditor<T> editor = new GenericObjectEditor<T>(classe, null);
				Optional<T> res = editor.showAndWait();

				if(res.isPresent())
				{
					observableList.add(res.get());
					getTableView(null, null, false).refresh();
				}
			}
		});

		return addButton; 
	}

	/**
	 * Gets the label used to show informations on the screen.
	 * @return The label used to show informations on the screen.
	 * @see Label
	 */
	public Label getInformationLabel()
	{
		//If the Label doesn't exist yet, creates it.
		if(informationLabel == null)
		{
			informationLabel = new Label("Double click to edit an item.\nYou can click on the header of each collumns to sort the table.\nYour changes are only applied to the deck when you click on the apply Button at the bottom of this screen.");
			informationLabel.setWrapText(true);
		}
		return informationLabel; 
	}

	/**
	 * Gets the Button used to go back to the MainMenu.
	 * @return The Button used to go back to the MainMenu.
	 * @see Button
	 */
	public Button getBackButton()
	{
		//If the Button doesn't exist yet, creates it.
		if(backButton == null)
		{
			backButton = new Button("Back");
			backButton.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0) {
					//mp.pause();
					Main.getStage().setScene(Main.getMainMenu());		
				}
			});
		}
		return backButton; 
	}

	/**
	 * Gets the button used to apply all the modifications to the originalList.
	 * @return The button used to apply all the modifications to the originalList.
	 * @see Button
	 */
	public Button getApplyButton()
	{
		//If the Button doesn't exist yet, creates it.		
		if(applyButton == null)
		{
			applyButton = new Button("Apply");

			//Upon applying the modifications, ask for confirmation then do accordingly to the selected class.
			applyButton.setOnAction(new EventHandler<ActionEvent>() {
				@SuppressWarnings("unchecked")
				@Override
				public void handle(ActionEvent arg0) {
					Dialog<ButtonType> dialogP = new Dialog<>();
					dialogP.setTitle("Apply ?");
					dialogP.setHeaderText("Do you wish to apply all the changes you made to the deck ?");
					ButtonType applyButtonType = new ButtonType("Apply", ButtonData.OK_DONE);
					ButtonType cancelButtonType = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
					dialogP.getDialogPane().getButtonTypes().addAll(applyButtonType, cancelButtonType);
					Optional<ButtonType> result = dialogP.showAndWait();
					if (result.get() == applyButtonType)
					{
						if(classe == Question.class)
						{
							Deck.getInstance().setQuestions((List<Question>) tv.getItems());
						}
						else if(classe == Account.class)
						{
							AccountsManager.setAccounts((List<Account>) tv.getItems());
						}
						Main.getStage().setScene(Main.getMainMenu());	
					}			
				}
			});
		}
		return applyButton; 
	}

	/**
	 * Gets the TableView of the provided type loaded with the provided list.
	 * @param list The list of objects we want to load the TableView with.
	 * @param c The class we want to use when creating the TableView.
	 * @param asNew If the TableView should be recreated or not.
	 * @param <T> The type of the objects we are editing.
	 * @return The TableView of the provided type loaded with the provided list.
	 * @see TableView
	 */
	public <T> GenericTableView<?> getTableView(List<T> list, Class<T> c, boolean asNew)
	{
		if(asNew)
		{
			observableList = FXCollections.observableArrayList(list);
			tv = new GenericTableView<T>(observableList, c);
			tv.setPrefWidth(Main.getStageWidth()*0.95);
		}

		return tv;
	}

	public void resize() {
		// TODO Auto-generated method stub

	}


}
