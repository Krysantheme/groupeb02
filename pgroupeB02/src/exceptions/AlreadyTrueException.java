package exceptions;

public class AlreadyTrueException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AlreadyTrueException() {
		super("there's is already a true choice");
	}

}
