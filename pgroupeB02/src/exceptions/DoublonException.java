package exceptions;

import modele.Question;

public class DoublonException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DoublonException(Question o) {
		super(o.toString()+"est d�j� pr�sent dans la liste");
		
	}

}
