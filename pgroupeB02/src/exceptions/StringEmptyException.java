package exceptions;

public class StringEmptyException extends Exception {
	
	public StringEmptyException() {
		super("This String can't be empty");
	}

}
