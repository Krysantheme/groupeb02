package exceptions;

public class TooManyChoicesException extends Exception {
	public TooManyChoicesException() {
		super("There is already four choices");
	}

}
