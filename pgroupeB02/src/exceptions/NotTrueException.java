package exceptions;

public class NotTrueException extends Exception {
	public NotTrueException() {
		super("There isn't any true answer in the choices");
	}
}
