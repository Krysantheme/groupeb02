package JUnit;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import exceptions.AlreadyTrueException;
import exceptions.DoublonException;
import exceptions.StringEmptyException;
import exceptions.TooManyChoicesException;
import modele.Account;
import modele.Deck;
import modele.GameObservable;
import modele.Palier;
import modele.Question;
import modele.Round;
import modele.Score;
import modele.ScoreBoard;
import modele.State;
import test.Explorateur;
import util.AccountsManager;

public class TestUnitaire{
	Deck deck;
	Score score;
	ScoreBoard sboard;
	Account acc;
	GameObservable game;
	Question q1,q1Biss,q2,q3,q;
	Palier p;
	List<Question> questions;
	@SuppressWarnings("unchecked")
	@Before
	public void initialisation() throws Exception{
		acc=new Account("re", "tes");
		sboard=new ScoreBoard();
		score=new Score("test", 10);
		deck=Deck.getInstance();
		questions=(List<Question>) Explorateur.getField(deck, "questions");
		Map<String, Boolean> mp=new HashMap<>();
		mp.put("c1", true);
		mp.put("c2", false);
		mp.put("c3", false);
		mp.put("c4", false);
		q=new Question("v","i",Round.FIRST_ROUND,mp);	
		q1=new Question("t","i",Round.FIRST_ROUND,mp);
		q1Biss=new Question("t","i",Round.FIRST_ROUND,mp);
		q2=new Question("i","w",Round.LAST_ROUND,mp);
		q3=new Question("f","ff�",Round.SECOND_ROUND,mp);
		p=Palier.PALIER_1;
	}
	
	@Test
	public void testGetScore() {
		assertArrayEquals(sboard.getScore(), (Score[])Explorateur.getField(sboard, "listScore"));
	}
	@Test
	public void testIsLowerTo() {
		Score s=new Score("tes",50);
		assertTrue(score.isLowerTo(s));
		assertFalse(s.isLowerTo(score));
	}
	@Test
	public void testSetMoney() {
		score.setMoney(5);
		assertEquals(5, (int)Explorateur.getField(score, "money"));
	}
	@Test
	public void testGetMoney() {
		assertEquals(score.getMoney(), (int)Explorateur.getField(score, "money"));
	}
	@Test
	public void testSetNickName() {
		score.setNickName("new");
		assertEquals("new", (String)Explorateur.getField(score, "nickName"));
	}
	@Test
	public void testGetNickName() {
		assertEquals(score.getNickName(), (String)Explorateur.getField(score, "nickName"));
	}
	@Test
	public void testSetCurrency() {
		acc.setCurrency(30);
		assertEquals(30, (int)Explorateur.getField(acc, "currency"));
	}
	@Test
	public void testSetAdmin() {
		acc.setIsAdministrator(true);
		assertEquals(true, (Boolean)Explorateur.getField(acc, "isAdministrator"));
	}
	@Test
	public void testSetPassword() {
		acc.setPassword("new");
		assertEquals("new", (String)Explorateur.getField(acc, "password"));
	}
	@Test
	public void testSetNickname() {
		acc.setNickname("new");
		assertEquals("new", (String)Explorateur.getField(acc, "nickname"));
	}
	@Test
	public void testGetPassword() {
		assertEquals(acc.getPassword(), (String)Explorateur.getField(acc, "password"));
	}
	@Test
	public void testGetNickname() {
		assertEquals(acc.getNickname(), (String)Explorateur.getField(acc, "nickname"));
	}
	@Test
	public void testGetCurrency() {
		assertEquals(acc.getCurrency(), (int)Explorateur.getField(acc, "currency"));
	}
	@Test
	public void testAddCurrency() {
		int old=(int)Explorateur.getField(acc, "currency"),i=3;
		acc.addCurency(i);
		assertEquals(old+i, (int)Explorateur.getField(acc, "currency"));
	}
	@Test
	public void testGetIsAdmin() {
		assertEquals(acc.getIsAdministrator(), (Boolean)Explorateur.getField(acc, "isAdministrator"));
	}
	@Test
	public void testGetCorrectAnswer() {
		game=new GameObservable();
		assertEquals(game.getCorrectAnswer(),(String)Explorateur.getField(game, "correctAnswer"));
		game=null;
	}
	@Test
	public void testNextPalier() {
		game=new GameObservable();
		Palier p1;
		assertEquals(game.getPalier(),(Palier)Explorateur.getField(game, "currentPalier"));
		p1=(Palier)Explorateur.getField(game, "currentPalier");
		game.nextPalier();
		assertEquals(game.getPalier(),(Palier)Explorateur.getField(game, "currentPalier"));
		assertNotEquals(p1, (Palier)Explorateur.getField(game, "currentPalier"));
		game=null;
	}
	@Test
	public void testGetPalier() {
		game=new GameObservable();
		assertEquals(game.getPalier(),(Palier)Explorateur.getField(game, "currentPalier"));
		game=null;
	}
	@SuppressWarnings("unchecked")
	@Test
	public void testGetChoisesMap() {
		game=new GameObservable();
		assertEquals(game.getChoisesMap(),(Map<String,Boolean>)Explorateur.getField(game, "mp"));
		game=null;
	}
	@SuppressWarnings("unchecked")
	@Test
	public void testGetChoises() {
		game=new GameObservable();
		List<String> li=new ArrayList<>();
		li.add("1");
		li.add("1");
		game.setChoises(li);
		assertEquals(game.getChoises(), (List<String>)Explorateur.getField(game, "listChoix"));
		game=null;
	}
	@SuppressWarnings("unchecked")
	@Test
	public void testSetChoises() {
		game=new GameObservable();
		List<String> li=new ArrayList<>();
		li.add("1");
		li.add("1");
		game.setChoises(li);
		assertEquals(li, (List<String>)Explorateur.getField(game, "listChoix"));
		game=null;
	}
	@Test
	public void testGetStatementIntoGame() {
		game=new GameObservable();
		assertEquals(game.getStatement(), (String)Explorateur.getField(game, "statement"));
		game=null;
	}
	@Test
	public void testGetAuthorIntoGame() {
		game=new GameObservable();
		assertEquals(game.getAuthor(), (String)Explorateur.getField(game, "author"));
		game=null;
	}
	@Test
	public void testGetQuestionIntoGame() {
		game=new GameObservable();
		assertEquals(game.getQuestion(), (Question)Explorateur.getField(game, "question"));
		game=null;
	}
	@Test
	public void testGetDeck(){
		game=new GameObservable();
		assertEquals(game.getDeck(), (Deck)Explorateur.getField(game, "deck"));
		game=null;
	}
	@Test
	public void testGetEtat() {
		game=new GameObservable();
		assertEquals(game.getEtat(), (State)Explorateur.getField(game,"etat"));
		game=null;
	}
	@Test
	public void testSetEtat() {
		game=new GameObservable();
		game.nextPalier();
		game.setEtat(State.WIN);
		assertEquals(State.WIN, (State)Explorateur.getField(game,"etat"));
		game=null;
	}
	@Test
	public void testGetMoneySave() {
		game=new GameObservable();
		game.nextPalier();
		game.setEtat(State.WIN);
		assertEquals(game.getMoneySave(), (int)Explorateur.getField(game, "moneySave"));
		game=null;
	}
	@Test
	public void testGetAnswerFriend() {
		game=new GameObservable();
		String str="TestAnswerFriend";
		game.setAnswerFriend(str);
		assertEquals(game.getAnswerFriend(), (String)Explorateur.getField(game, "answerFriend"));
		game=null;
	}
	@Test
	public void testSetAnswerFriend() {
		game=new GameObservable();
		String str="TestAnswerFriend";
		game.setAnswerFriend(str);
		assertNotNull((String)Explorateur.getField(game, "answerFriend"));
		assertEquals(str, (String)Explorateur.getField(game, "answerFriend"));
		game=null;
	}
	@SuppressWarnings("unchecked")
	@Test
	public void testSetListHelpPublic() {
		game=new GameObservable();
		List<Integer> li=new ArrayList<>();
		li.add(1);
		li.add(1);
		li.add(1);
		li.add(1);
		game.setListHelpPublic(li);
		assertEquals(li, (List<Integer>)Explorateur.getField(game, "listHelpPublic"));
		game=null;
	}
	@Test
	@SuppressWarnings("unchecked")
	public void testGetListHelpPublic() {
		game=new GameObservable();
		List<Integer> li=new ArrayList<>();
		li.add(1);
		li.add(1);
		li.add(1);
		li.add(1);
		game.setListHelpPublic(li);
		assertEquals(game.getListHelpPublic(), (List<Integer>)Explorateur.getField(game, "listHelpPublic"));
		li.add(1);
		game.setListHelpPublic(li);
		assertEquals(game.getListHelpPublic(), (List<Integer>)Explorateur.getField(game, "listHelpPublic"));

		game=null;
	}
	@Test
	public void testPalier() {
		int somme=(int)Explorateur.getField(p, "somme");
		Round rd=(Round)Explorateur.getField(p, "round");
		assertEquals(p.getRound(), rd);
		assertEquals(p.getSomme(), somme);
		
	}
	@SuppressWarnings("deprecation")
	@Test
	public void testAdd(){
		try {
			deck.add(q1);
		} catch (DoublonException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		assertTrue(questions.contains(q1));
		try {
			deck.add(q1Biss);
		} catch (DoublonException e) {
			// TODO Auto-generated catch block
		}
		questions.remove(q1);
	}
	@Test
	public void testGetQuestions() {
		assertTrue(deck.getQuestions().equals(questions));
	}
	@Test
	public void testRemove() {
		try {
			deck.add(q1);
		} catch (DoublonException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(deck.remove(q1));
		assertFalse(questions.contains(q1));
	}
	@Test
	public void testGetQuestion() {
		try {
			deck.add(q1);
		} catch (DoublonException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(q1.equals(deck.getQuestion(questions.indexOf(q1))));
		questions.remove(q1);
		assertNull(deck.getQuestion(-1));
	}
	@Test
	public void testUpdate() {
		try {
			deck.add(q1);
		} catch (DoublonException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(questions.contains(q1));
		int index=questions.indexOf(q1);
		assertFalse(deck.update(q1, q1));
		deck.update(q1, q2);
		assertFalse(questions.contains(q1));
		assertTrue(questions.contains(q2));
		assertEquals(index, questions.indexOf(q2));
		deck.remove(q2);
	}
	@Test
	public void testClone() {
		assertEquals(deck.clone(), questions);
	}
	@SuppressWarnings("unchecked")
	@Test
	public void setSet() {
		List<Question> list=deck.clone();
		List<Question> li=new ArrayList<>();
		li.add(q1);
		deck.set(li);
		questions=(List<Question>) Explorateur.getField(deck, "questions");
		assertEquals(questions,li);
		deck.set(list);
		questions=(List<Question>)Explorateur.getField(deck, "questions");
	}
	@Test
	public void testSizeQuestions() {
		assertEquals(questions.size(), deck.sizeQuestions());
	}
	@Test
	public void testCompareRound() {
		assertTrue(deck.compareRound(Round.FIRST_ROUND,q1));
		assertFalse(deck.compareRound(Round.LAST_ROUND,q1));
	}
	@Test
	public void testRemoveChoice() {
		Map<String, Boolean> mp=(Map<String,Boolean>)Explorateur.getField(q, "choices");
		try {
			q.removeChoice("c1");
		} catch (StringEmptyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNull(mp.get("c1"));
		try {
			q.addChoice("c1", true);
		} catch (AlreadyTrueException | StringEmptyException | TooManyChoicesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void testGetAuthor() {
		String author=(String)Explorateur.getField(q, "author");
		assertEquals(q.getAuthor(), author);
	}
	@Test
	public void testGetStatement() {
		String statement=(String)Explorateur.getField(q, "statement");
		assertEquals(q.getStatement(), statement);
	}
	@Test
	public void testGetRound() {
		Round round=(Round)Explorateur.getField(q, "round");
		assertEquals(q.getRound(), round);
	}
	@Test
	public void testGetChoices() {
		Map<String,Boolean> mp=(Map<String,Boolean>)Explorateur.getField(q, "choices");
		assertEquals(q.getChoices(), mp);
	}
	@Test
	public void testUse() {
		q.use();
		assertTrue((boolean)Explorateur.getField(q, "isUsed"));
	}
	@Test
	public void testHaveBeenUsed() {
		q.use();
		assertEquals(q.haveBeenUsed(), (boolean)Explorateur.getField(q, "isUsed"));
	}
	@After
	public void tearDown() throws Exception{
		deck=null;
		questions=null;
		q1=null;
		q1Biss=null;
		q2=null;
		q3=null;
		q=null;
		game=null;
		AccountsManager.removeAccount(acc);
	}
	
}
