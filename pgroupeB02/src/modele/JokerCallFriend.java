package modele;

import java.util.Random;

public class JokerCallFriend implements IJokerStrategy {

	@Override
	public void useJoker(GameObservable g) {
		// TODO Auto-generated method stub
		Random rand=new Random();
		int i=rand.nextInt(10);
		//choose the method to call
		//60% of luck to get the true value from the friend
		//30% of luck to get a not sure value from the friend
		//10% of luck to get a unknow value from the friend
		switch(i) {
		case 0:
			g.setAnswerFriend(sure(g));
			break;
		case 1:
			g.setAnswerFriend(sure(g));
			break;
		case 2:
			g.setAnswerFriend(sure(g));
			break;
		case 3:
			g.setAnswerFriend(sure(g));
			break;
		case 4:
			g.setAnswerFriend(sure(g));
			break;
		case 5:
			g.setAnswerFriend(sure(g));
			break;
		case 6:
			g.setAnswerFriend(notSure(g));
			break;
		case 7:
			g.setAnswerFriend(notSure(g));
			break;
		case 8:
			g.setAnswerFriend(notSure(g));
			break;
		case 9: 
			g.setAnswerFriend(unknow(g));
			break;
		}
	}
	private String unknow(GameObservable g) {
		String[] str= {
			"I don't know but try",
			"I have never heard that, it's",
			"I don't know, but I'll choose the"
		};
		String[] list= {"A","B","C","D"};
		Random rand=new Random();
		//choose a random statement and a random letter
		return str[rand.nextInt(str.length)]+" "+list[rand.nextInt(list.length)];
	}
	private String sure(GameObservable g) {
		String[] str= {
			"I'm sure the good answer is the",
			"I know the good answer,",
			"Trust me, it's"
		};
		String[] list= {"A","B","C","D"};
		int i=0;
		for(String s:g.getChoisesMap().keySet()) {
			//search true value
			if(g.getChoisesMap().get(s))
				break;
			i++;
		}
		Random rand=new Random();
		//choose a random statement plus add the letter of the good answer
		return str[rand.nextInt(str.length)]+" "+list[i];
		
	}
	private String notSure(GameObservable g) {
		String[] str= {
				"I'm not sure but I think the good answer is",
				"That should be",
				"I think it's"
		};
		String[] list= {"A","B","C","D"};
		int i=0;
		for(String s:g.getChoisesMap().keySet()) {
			//search the true value
			if(g.getChoisesMap().get(s))
				break;
			i++;
		}
		Random rand=new Random();
		//70% of luck to choose a random statement plus the true value
		if(!(rand.nextInt(10)>7))
			return str[rand.nextInt(str.length)]+" "+list[i];
		//30% of luck to choose a random statement plus a random value
		return str[rand.nextInt(str.length)]+" "+list[rand.nextInt(4)];
	}

}
