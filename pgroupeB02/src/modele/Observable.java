package modele;

import java.util.ArrayList;
import java.util.List;

import view.Observer;

public abstract class Observable {
	List<Observer> listObservers;
	public Observable() {
		listObservers=new ArrayList<>();
	}
	public void ajouter(Observer o) {
		listObservers.add(o);
	}
	public void retirer(Observer o) {
		listObservers.remove(o);
	}
	public void notifie() {
		for(Observer o:listObservers) {
			o.actualise(this);
		}
	}

}
