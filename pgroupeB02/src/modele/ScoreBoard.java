package modele;

import java.io.FileNotFoundException;
import java.util.Arrays;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import util.Serialization;

public class ScoreBoard {
	private Score[] listScore;

	public ScoreBoard() {
		listScore=new Score[10];
	}
	public Score[] getScore() {
		Score[] sc =new Score[10];
		int i=0;
		for(Score s:listScore) {
			sc[i]=s;
			i++;
		}
		return sc;
		
	}
	public void saveScoreBoard(String fileName) {
		Serialization.writeToJson(this, fileName, this.getClass());
	}
	public void loadScoreBoard(String fileName)
	{
		//Load all the questions from the json file into the questions list
		try {
			listScore = ((ScoreBoard)Serialization.readFromJson(fileName,ScoreBoard.class)).listScore;
			
		} catch (FileNotFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("An error occured while trying to read the questions from the score data file");
			alert.setContentText("Please make sure that the questions data file is placed in the file directory.");
			alert.showAndWait();
		}
		
	}
	public void addScore(Score s) {
		if(containsNickName(s)) {
			for(Score sc:listScore) {
				if(sc.getNickName().equals(s.getNickName())) {
					if(sc.getMoney()<s.getMoney())
						sc.setMoney(s.getMoney());
				}
			}
			sort();
			return;
		}
		if(listScore[9].getMoney()<s.getMoney()) {
			listScore[9]=s;
			sort();
			return;
		}
	}
	public Score get(int index) {
		return listScore[index];
	}
	public void sort() {
		for(int j=0;j<listScore.length-1;j++) {
			for(int z=j+1;z<listScore.length;z++) {
				if(listScore[j].getMoney()<listScore[z].getMoney()) {
					Score temp=listScore[j];
					listScore[j]=listScore[z];
					listScore[z]=temp;
				}
			}
		}
	}
	public boolean containsNickName(Score s) {
		for(Score sc:listScore) {
			if(sc.getNickName().equals(s.getNickName()))
				return true;
		}
		return false;
	}
	public int indexOf(Score s) {
		if(containsNickName(s)) {
			int i=0;
			for(Score sc:listScore) {
				if(sc.getNickName().equals(s.getNickName())) {
					return i;
				}
				i++;
			}
		}
		return -1;
	}
	@Override
	public String toString() {
		return "ScoreBoard [listScore=" + Arrays.toString(listScore) + "]";
	}
	
	
}
