package modele;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class JokerCallPublic implements IJokerStrategy {

	@Override
	public void useJoker(GameObservable g) {
		// TODO Auto-generated method stub
		
		Random rand=new Random();
		List<Integer> li=new ArrayList<>();
		int i,compteur=70;
		
		
		int n=0;
		//count the number of choises
		for(String str:g.getChoises()) {
			if(str.equals("")){
				n++;
			}
		}
		//if the player has already use the JokerFiftyFifty
		if(n==2) {
			for(String str:g.getChoises()) {
				if(str.equals(""))
					li.add(0);
				else {
					//search the true value
					if(g.getChoisesMap().get(str)) {
						//compteur hasen't decrease yet
						if(compteur==70) {
							i=rand.nextInt(compteur)+1;
							compteur-=i;
							//add 30 for the true value plus a random value
							i+=30;
							li.add(i);
						}
						else {
							//add 30 plus the rest of compteur
							li.add(compteur+30);
						}
					}
					else {
						if(compteur==70) {
							//compteur hasen't decrease yet
							i=rand.nextInt(compteur)+1;
							li.add(i);
							compteur-=i;
						}
						else {
							//add the rest of compteur
							li.add(compteur);
						}
					}
				}
			}
		}
		//if there are 4 choises
		else {
			List<String> l=g.getChoises();
			for(int j=0;j<g.getChoises().size()-1;j++) {
				//set a random value
				i=rand.nextInt(compteur)+1;
				//search the true value
				if(g.getChoisesMap().get(l.get(j)))
					//add 3 for the true value
					li.add(i+30);
				else
					//add
					li.add(i);
				compteur-=i;
			}
			if(g.getChoisesMap().get(l.get(3)))
				//add the rest of compteur plus 30 for the true value
				li.add(compteur+30);
			else
				//add the rest of the value
				li.add(compteur);
		}
		g.setListHelpPublic(li);
	}

}
