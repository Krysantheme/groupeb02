package modele;


import util.AccountsManager;

public class Account implements Comparable<Account>
{
	private String nickname;
	private String password;
	private boolean isAdministrator = false;
	private int currency = 0;

	public Account(String nickname,String password)
	{
		this.nickname = nickname;
		this.password = password;

		//Add the account into the accounts manager
		AccountsManager.addAccount(this);
	}

	public Account(String nickname,String password, boolean admin)
	{
		this.nickname = nickname;
		this.password = password;
		this.isAdministrator = admin;

		//Add the account into the accounts manager
		AccountsManager.addAccount(this);
	}

	public Account(String nickname,String password, boolean admin, int currency)
	{
		this.nickname = nickname;
		this.password = password;
		this.isAdministrator = admin;
		this.currency = currency;
		
		//Add the account into the accounts manager
		AccountsManager.addAccount(this);
	}
	/**
	 * Copy and get data from the account
	 * @param acc
	 */
	public void copyDataFromAccount(Account acc)
	{
		//Set user's values with a copy of another one user
		this.nickname = acc.getNickname();
		this.password = acc.getPassword();
		this.isAdministrator = acc.isAdministrator;
		this.currency = acc.getCurrency();
	}
	
	/**
	 * Returns permission status
	 * @return
	 */


	public boolean getIsAdministrator()
	{
		return this.isAdministrator;
	}
	/**
	 * Add a certain number of Currencies defined by "value" to the player
	 * @param value
	 */

	public void addCurency(int value)
	{
		this.currency += value;
	}
	/**
	 * Returns the total amount of currency obtained by the account
	 * @return
	 */

	public int getCurrency()
	{
		return this.currency;
	}

	/**
	 * Returns the nickname of the account
	 * @return
	 */

	public String getNickname()
	{
		return this.nickname;
	}
 
	/**
	 * Returns the password of the account
	 * @return
	 */
 

 
	public String getPassword()
	{
		return this.password;
	}

 
	/**
	 * Set a nickname
	 * @param nickname
	 */
 

 
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * Set a password
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * Set an account as Administrator
	 * @param isAdministrator
	 */
	public void setIsAdministrator(boolean isAdministrator) {
		this.isAdministrator = isAdministrator;
	}
	/**
	 * Set total value of the account's wallet
	 * @param currency
	 */
	public void setCurrency(int currency) {
		this.currency = currency;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (nickname == null) {
			if (other.nickname != null)
				return false;
		} else if (!nickname.equalsIgnoreCase(other.nickname))
			return false;
		return true;
	}

	@Override
	public int compareTo(Account p) {
		return nickname.compareToIgnoreCase(p.getNickname());
	}

	public String toString()
	{
		return this.nickname;
	}
}
