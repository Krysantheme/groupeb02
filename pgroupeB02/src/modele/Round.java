package modele;

public enum Round {
	FIRST_ROUND ("Easy"),
	SECOND_ROUND ("Normal"),
	LAST_ROUND ("Hard");
	private String level;
	
	Round(String lvl){
		this.level=lvl;
	}
	public String toString() {
		return this.level;
	}
}
