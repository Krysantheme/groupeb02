
package modele;

public class Score {
	private String nickName;
	private int money;
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + money;
		result = prime * result + ((nickName == null) ? 0 : nickName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Score other = (Score) obj;
		if (money != other.money)
			return false;
		if (nickName == null) {
			if (other.nickName != null)
				return false;
		} else if (!nickName.equals(other.nickName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Score [nickName=" + nickName + ", money=" + money + "]";
	}
	public Score(String nickName, int money) {
		super();
		this.nickName = nickName;
		this.money = money;
	}
	public boolean isLowerTo(Score s) {
		if(money<s.money)
			return true;
		return false;
	}
	public Score clone() {
		return new Score(nickName,money);
	}
	
}
