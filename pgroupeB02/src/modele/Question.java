package modele;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import exceptions.AlreadyTrueException;
import exceptions.NotTrueException;
import exceptions.StringEmptyException;
import exceptions.TooManyChoicesException;

public class Question implements Serializable{
	private String author,statement;
	private Round round;
	private Map<String,Boolean> choices;
	private transient boolean isUsed;
	
	/**
	 *  Return the author of the question
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	
	/**
	 * Set the author of a question
	 * @param author the author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public Question clone() {
		try {
			return new Question(getAuthor(),getStatement(),getRound(),getChoices());
		} catch (TooManyChoicesException | AlreadyTrueException | NotTrueException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Return the statement of the question
	 * @return the statement
	 */
	public String getStatement() {
		return statement;
	}
	
	/**
	 * Set the statement of a question
	 * @param statement the statement
	 */
	public void setStatement(String statement) {
		this.statement = statement;
	}
	
	/**
	 * Return the Round of the question
	 * @return the Round
	 */
	public Round getRound() {
		return round;
	}
	
	/**
	 * Set the round of a question
	 * @param round the round
	 */
	public void setRound(Round round) {
		this.round = round;
	}
	
	/**
	 * Return the answer choices of the question 
	 * @return the answers
	 */
	public Map<String, Boolean> getChoices() {
		Map<String,Boolean> mp=new HashMap<>();
		mp.putAll(choices);
		return mp;
	}
	
	/**
	 * Set the Answer Choices of a question
	 * @param choices the choices map
	 * @throws TooManyChoicesException if there are more than 4 choices
	 * @throws AlreadyTrueException if there are more than one true choice
	 * @throws NotTrueException there isn't any true choice
	 */
	public void setChoices(Map<String, Boolean> choices)throws  TooManyChoicesException, AlreadyTrueException, NotTrueException {
		Map<String,Boolean> mp=new HashMap<>();
		int i=0;
		for(Boolean bool:choices.values()) {
			if(bool==true)
				i++;
		}
		if(i>1)
			throw new AlreadyTrueException();
		if(choices.size()>4)
			throw new TooManyChoicesException();
		if(choices.size()==4 && i==0)
			throw new NotTrueException();
		mp.putAll(choices);
		this.choices = mp;
		
	}
	/**
	 * add a new choice into the Choices of question
	 * @param c	is the answer 
	 * @param is is the boolean of the answer
	 * @throws AlreadyTrueException	if there is already a true answer and the new answer is also true
	 * @throws StringEmptyException if the new answer is empty
	 * @throws TooManyChoicesException if there is 4 answer in the choices
	 */
	public void addChoice(String c,Boolean is)throws AlreadyTrueException ,StringEmptyException,TooManyChoicesException{
		if(c.isEmpty())
			throw new StringEmptyException();
		if(choices.size()>=4)
			throw new TooManyChoicesException();	
		if(is==false) {
			choices.put(c, is);
		}
		for(Boolean bool:choices.values()) {
			if(bool==true)
				throw new AlreadyTrueException();
		}
		choices.put(c, is);
	}
	/**
	 * remove a answer from the choices
	 * @param c is the answer
	 * @return true if the answer was correctly remove
	 * @throws StringEmptyException if the answer gave is empty
	 */
	public boolean removeChoice(String c)throws StringEmptyException {
		if(c.isEmpty())
			throw new StringEmptyException();
		return choices.remove(c);
	}
	/**
	 * constructor
	 * @param author
	 * @param statement
	 * @param round
	 * @param choices
	 * @throws NotTrueException 
	 * @throws AlreadyTrueException 
	 * @throws TooManyChoicesException 
	 */
	public Question(String author, String statement, Round round, Map<String, Boolean> choices) throws TooManyChoicesException, AlreadyTrueException, NotTrueException {

		setAuthor(author);
		setStatement(statement);
		setRound(round);
		setChoices(choices);
	}
	/**
	 * constructor
	 * @param author
	 * @param statement
	 * @param round
	 */
	public Question(String author,String statement,Round round) {
		setAuthor(author);
		setStatement(statement);
		setRound(round);
	}
	
	/**
	 * Return true if the question have been used
	 * @return
	 */
	public boolean haveBeenUsed()
	{
		return isUsed;
	}
	
	/**
	 * Use a question
	 */
	public void use()
	{
		this.isUsed = true;
	}
	
	
	
	public boolean equals(Object o) {
		if(o instanceof Question) {
			Question q=(Question)o;
			
			return q.getStatement().equals(getStatement()) && q.getAuthor().equals(getAuthor());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "Question [author=" + author + ", statement=" + statement + ", round=" + round + ", choices=" + choices.toString()
				+ "]";
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	
	
	
}
