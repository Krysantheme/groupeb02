package modele;

public enum Palier {
	PALIER_1(100,Round.FIRST_ROUND),
	PALIER_2(200,Round.FIRST_ROUND),
	PALIER_3(300,Round.FIRST_ROUND),
	PALIER_4(500,Round.FIRST_ROUND),
	PALIER_5(1000,Round.FIRST_ROUND),
	PALIER_6(2000,Round.SECOND_ROUND),
	PALIER_7(4000,Round.SECOND_ROUND),
	PALIER_8(8000,Round.SECOND_ROUND),
	PALIER_9(16000,Round.SECOND_ROUND),
	PALIER_10(32000,Round.SECOND_ROUND),
	PALIER_11(64000,Round.LAST_ROUND),
	PALIER_12(125000,Round.LAST_ROUND),
	PALIER_13(250000,Round.LAST_ROUND),
	PALIER_14(500000,Round.LAST_ROUND),
	PALIER_15(1000000,Round.LAST_ROUND);
	private int somme;
	private Round round;
	Palier(int i,Round round) {
		this.somme=i;
		this.round=round;
	}
	public String toString() {
		return this.somme+" �";
	}
	public Round getRound() {
		return this.round;
	}
	public int getSomme() {
		return this.somme;
	}
}
