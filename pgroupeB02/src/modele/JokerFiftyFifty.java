package modele;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class JokerFiftyFifty implements IJokerStrategy {
	
	@Override
	public void useJoker(GameObservable g) {
		// TODO Auto-generated method stub
		Map<String,Boolean> mp=g.getChoisesMap();
		Random rand=new Random();
		int tr=0,i=0;
		List<String> li=new ArrayList<>();
		for(String str:mp.keySet()) {
			li.add(str);
			//search the true value
			if(mp.get(str)) {
				i=tr;
			}
			//count the number of choises
			tr++; 
		}
		int r=rand.nextInt(4);
		//search the others value to keep with the true value
		while(r==i) {
			r=rand.nextInt(4);
		}
		
		for(int j=0;j<mp.size();j++) {
			//set the 2 others value ""
			if(r!=j && i!=j) {
				li.set(j,"");
			}
		}
		//set the list of choises
		g.setChoises(li);
	}
}
