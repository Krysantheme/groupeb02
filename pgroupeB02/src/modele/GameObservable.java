package modele;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import exceptions.AlreadyTrueException;
import exceptions.NotTrueException;
import exceptions.TooManyChoicesException;

public class GameObservable extends Observable {
	private Map<Palier,Question> listQuestion;
	private Deck deck;
	private Palier currentPalier;
	private State etat;
	private String correctAnswer,answerFriend,statement,author;
	private int moneySave;
	private Question question;
	private List<Integer> listHelpPublic;
	private List<String> listChoix;
	private Map<String,Boolean> mp;
	/**
	 * constructor
	 */
	public GameObservable() {
		deck=Deck.getInstance();
		deck.loadQuestions("test");
		listQuestion=new HashMap<>();
		currentPalier=Palier.PALIER_1;
		this.fill();
		setChoises();
		etat=State.EN_COURS;
	}
	public List<Integer> getListHelpPublic(){
		return this.listHelpPublic;
	}
	public void setListHelpPublic(List<Integer> li) {
		listHelpPublic=new ArrayList<>();
		if(li.size()==4)
			listHelpPublic.addAll(li);
		else { 
			for(Integer d:li) {
				listHelpPublic.add(d);
				if(listHelpPublic.size()==4)break;
			}
		}
	}
	
	public String getAnswerFriend() {
		return answerFriend;
	}
	public void setAnswerFriend(String answerFriend) {
		this.answerFriend = answerFriend;
	}
	/**
	 * 
	 * @return the state of the gamr
	 */
	public State getEtat() {
		return etat;
	}
	
	public void setEtat(State etat) {
		if(currentPalier.equals(Palier.PALIER_1))
			return;
		this.etat = etat;
		//setMoneySave(currentPalier.getSomme());
		int i=0;
		for(Palier r:Palier.values()) {
			if(currentPalier.equals(r)) {
				setMoneySave(Palier.values()[i-1].getSomme());
				break;
			}
			i++;
			
		}
		notifie();
		
	}
	public int getMoneySave() {
		return moneySave;
	}

	private void setMoneySave(int moneySave) {
		this.moneySave = moneySave;
	}
	public Deck getDeck() {
		return this.deck;
	}
	public Question getQuestion() {
		question=this.listQuestion.get(currentPalier).clone();
		return question;
	}

	/**
	 * fill the map of the game
	 */
	public void fill() {
		Random rand=new Random();
		for(Palier r:Palier.values()) {
			
			List<Question> listTemp=deck.clone();

			Question choix=listTemp.get(rand.nextInt(listTemp.size()));
			while(listQuestion.values().contains(choix) || !deck.compareRound(r.getRound(), choix)) {
				choix=listTemp.get(rand.nextInt(listTemp.size()));
			}
			listQuestion.put(r, choix);
		}
	}
	/**
	 * 
	 * @return the statement of the current question
	 */
	public String getStatement() {
		statement=listQuestion.get(currentPalier).getStatement();
		return statement;
	}
	/**
	 * 
	 * @return the author of the current question
	 */
	public String getAuthor() {
		author=listQuestion.get(currentPalier).getAuthor();
		return author;
	}
	
	public void setChoises() {
		listChoix=new ArrayList<>();
		for(String str:listQuestion.get(currentPalier).getChoices().keySet()) {
			listChoix.add(str);
			if(listQuestion.get(currentPalier).getChoices().get(str))
				System.out.println(str);
		}
	}
	public void setChoises(List<String> str) {
		listChoix=str;
	}
	/**
	 * 
	 * @return the list of choices of the current question
	 */
	public List<String> getChoises(){
		return listChoix;
	}
	/**
	 * 
	 * @return the map of the current question
	 */
	public Map<String,Boolean> getChoisesMap(){
		mp=new HashMap<>();
		for(String str:listQuestion.get(currentPalier).getChoices().keySet()) {
			mp.put(str, listQuestion.get(currentPalier).getChoices().get(str));
		}
		return mp;
	}
	/**
	 * 
	 * @return the current palier
	 */
	public Palier getPalier() {
		return this.currentPalier;
	}
	/**
	 * set the next palier
	 * @return the current palier
	 */
	public Palier nextPalier() {
		int i=0;
		for(Palier r:Palier.values()) {
			if(i==Palier.values().length-1) {
				etat=State.WIN;
				setMoneySave(r.getSomme());
				return currentPalier;
			}
			if(currentPalier.equals(r)) {
				if(r.equals(Palier.PALIER_5))
					setMoneySave(Palier.PALIER_5.getSomme());
				if(r.equals(Palier.PALIER_10))
					setMoneySave(Palier.PALIER_10.getSomme());
				
				currentPalier=Palier.values()[i+1];
				setChoises();
				return currentPalier;
			}
			i++;
		}
		return currentPalier;
	}
	public void setJoker(IJokerStrategy joker) {
		joker.useJoker(this);
		notifie();
	}
	
	/**
	 * set a new map of choise for the current palier 
	 * @param mp
	 */
	public void setChoixMap(Map<String,Boolean> mp) {
		Question q=listQuestion.get(currentPalier);
		try {
			q.setChoices(mp);
		} catch (TooManyChoicesException | AlreadyTrueException | NotTrueException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		listQuestion.put(currentPalier, q);
	}
	/**
	 * check if the choise given is true or false
	 * @param str
	 */
	public void setChoix(String str) {
		if(str=="")
			return;
		if(listQuestion.get(currentPalier).getChoices().containsKey(str)) {
			if(listQuestion.get(currentPalier).getChoices().get(str)) {
				nextPalier();
			}
			else {
				etat=State.LOSE;
				for(String str1:listQuestion.get(currentPalier).getChoices().keySet()) {
					if(listQuestion.get(currentPalier).getChoices().get(str1)){
						setCorrectAnswer(str1);
					}
				}
			}
		}
		else {
			etat=State.LOSE;
			for(String str1:listQuestion.get(currentPalier).getChoices().keySet()) {
				if(listQuestion.get(currentPalier).getChoices().get(str1)){
					setCorrectAnswer(str1);
				}
			}
		}
		notifie();
	}
	/**
	 * 
	 * @return only if the game is lose however null
	 */
	public String getCorrectAnswer() {
		return correctAnswer;
	}
	/**
	 * set the correct answer
	 * @param correctAnswer
	 */
	private void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
}
