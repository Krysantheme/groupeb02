package modele;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.sun.javafx.collections.MappingChange.Map;

import exceptions.DoublonException;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import util.Serialization;

public class Deck {
	private transient static Deck instance;
	private transient int currentIndex = -1;
	private List<Question> questions;
	
	private Deck() {
		questions=new ArrayList<>();
	}
	/**
	 * Returns current instance.
	 * @return 
	 */
	public static Deck getInstance()
	{
		if(instance == null)
		{
			instance = new Deck();
		}

		return instance;
	}
	/**
	 * add the question in the deck
	 * @param q
	 * @throws DoublonException
	 */
	
	public List<Question> getQuestions(){
		return this.questions;
	}
	
	/**
	 * Sets the questions list
	 * @param 
	 */
	public void setQuestions(List<Question> qs)
	{
		questions = new ArrayList<>(qs);

		saveDeck("quizz.json");

		loadQuestions("quizz.json");
	}
	
	public void add(Question question) throws DoublonException {
		if(!questions.contains(question)) {
			questions.add(question.clone());
		}
		else
			throw new DoublonException(question);
	} 
	/**
	 * remove a question from the deck
	 * @param q is the question to remove
	 * @return true if the question is correctly removed
	 */
	public boolean remove(Question q) {
		return questions.remove(q);
	}
	
	/**
	 * Returns the question selected by it's index.
	 * @param index 
	 * @return
	 */
	public Question getQuestion(int index) {
		if(index >-1)
			return questions.get(index).clone();
		return null;
	}
	/**
	 * replace the question q1 by the question q2 into the list
	 * @param q1
	 * @param q2
	 * @return false if the new question wasn't add to the deck
	 */
	public boolean update(Question q1,Question q2) {
		if(find(q2)==-1) {
			int index=questions.indexOf(q1);
			if(index!=-1) {
				return questions.set(index, q2.clone()).equals(q1);
			}
		}
		return false;
	}
	/**
	 * get the index from the current question
	 * @param q
	 * @return index
	 */
	
	public int find(Question q) {
		return questions.indexOf(q);
	}
	
	public List<Question> clone(){
		List<Question> l=new ArrayList<>();
		for(Question q:questions) {
			l.add(q);
		}
		return l;
	}
	/**
	 * 
	 * @param set a new list of question 
	 */
	public void set(List<Question> l) {
		List<Question> li=new ArrayList<>();
		for(Question q:l) {
				li.add(q.clone());
		}
		questions=li;
		
	}
	
	
	
	/**
	 * Load all the questions from the json into the question list
	 * @param fileName
	 */
	public void loadQuestions(String fileName)
	{
		//Load all the questions from the json file into the questions list
		try {
			questions = ((Deck)Serialization.readFromJson(fileName,Deck.class)).questions;
			
		} catch (FileNotFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("An error occured while trying to read the questions from the questions data file");
			alert.setContentText("Please make sure that the questions data file is placed in the file directory.");
			alert.showAndWait();
		}
		
	}
	
	/**
	 * Save the current Deck into the given file
	 * @param fileName
	 */
	public void saveDeck(String fileName)
	{
		//Serialize the deck into the json file
		Serialization.writeToJson(this, fileName, this.getClass());
	}
	
	@Override
	public String toString() {
		if(questions.size()==0) {
			return "Deck is empty";
		}
		return "Deck [questions=" + questions + "]";
	}
	/**
	 * 
	 * @return the size of the list of questions
	 */
	public int sizeQuestions() {
		return questions.size();
	}
	/**
	 * compare the questions of two questions
	 * @param q
	 * @param p
	 * @return
	 */
	public boolean compareRound(Round q,Question p) {
		return q.equals(p.getRound());
	}
	
}
