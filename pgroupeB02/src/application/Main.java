package application;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import util.AccountsManager;
import view.Fenetre;

public class Main extends Application {
	
	private static Scene sceneMenu;
	private static Stage stageMenu;
	private static double stageHeight, stageWidth;
	private static ChangeListener<Number> resizeListener = (observable, oldValue, newValue) ->
	{
		if(sceneMenu != null)
		{
			stageHeight = sceneMenu.getHeight();
			stageWidth = sceneMenu.getWidth();
		}
		else
		{
			stageHeight = 990d;
			stageWidth = 600d;
		}
	};
	
	@Override
	public void start(Stage primaryStage) {
		try {
			Dialog<Stage> dialo=new Dialog<>();
			dialo.setResizable(true);
			final Window win=dialo.getDialogPane().getScene().getWindow();
			
			stageMenu = primaryStage;
			stageMenu=(Stage) win;
			stageMenu.sizeToScene();
			
			Fenetre fen=new Fenetre();
			AccountsManager.loadAccounts();
			sceneMenu = new Scene(fen, 990d, 600d);
			stageHeight = sceneMenu.getHeight(); 
			stageWidth = sceneMenu.getWidth();
			stageMenu.setTitle("Who wants to be a Millionaire ?");
			stageMenu.setResizable(true);
			stageMenu.setScene(sceneMenu);
			stageMenu.show();
			stageMenu.setOnCloseRequest(e->{
				Platform.exit();
			});
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		
		launch(args);
		
	}

	/**
	 * Returns for the stage height
	 * @return
	 */
	public static double getStageHeight()
	{
		return stageHeight;
	} 
	/**
	 * Returns the stage width
	 * @return
	 */
	public static double getStageWidth()
	{
		return stageWidth;
	}
	
	/**
	 * Returns the current stage
	 * @return
	 */
	public static Stage getStage()
	{
		return stageMenu;
	}

	public static Scene getMainMenu() {
		// TODO Auto-generated method stub
		return sceneMenu;
	}

}